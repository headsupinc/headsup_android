### What is this repository for? ###

* HeadsUp android application repository

* Version 1.0

### How do I get set up? ###

* Dependencies

    1. Download and Install Java 8 JDK:

        http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

    2. Download and Install Git:

        http://git-scm.com/download/win

    3. HeadsUp web service layer project is deployed and running

        https://bitbucket.org/vadimklochko/headsup/overview

* Summary of set up

    1. Download and Install the Motorola Device Manager:

        https://motorola-global-portal.custhelp.com/app/answers/detail/a_id/88481

    2. Download and Install Android Studio:

        http://developer.android.com/sdk/index.html

* Configuration

    1. Java 8 JDK Installation and Configuration:

        default installation (make note of the JDK installation folder location)

    2. Git Installation and Configuration:

        https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html

    3. Motorola Device Manager Installation and Configuration:

        default installation

    4. Android Studio Installation and Configuration: 

        default installation

* Android Studio Project configuration

    1. Launch Android Studio

    2. Select `Git` under `Check out project from Version Control` from Quick Start menu

        git repository url: `https://vadimklochko@bitbucket.org/vadimklochko/headsup_android.git`

        parent directory: `..\repos\headsup_android` (you may have to create these structures if they don't exist)

        directory name: `headsup_android`

    3. Click `Test` (if test fails, check the proxy settings in the `..\.gitconfig` file)

    4. Click `Clone`

    5. Confirm to open the project after cloning

        if you are prompted with `Error Loading Project` message(s) you may just have to install the missing platform(s) by clicking on the link(s) in the `Messages Gradle Sync` panel on the bottom

    6. Add the following options under `File` -> `Settings` -> `Build,Execution,Deployment` -> `Compiler`:

        command-line options: `--refresh-dependencies --recompile-scripts`

    7. Set JRE under `File` -> `Other Settings` -> `Default Settings` -> `Build,Execution,Deployment` -> `Build Tools` -> `Maven` -> `Runner`:

        jre: `Use Internal JRE`

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

    1. Source Control

        http://javapapers.com/android/android-studio-git-tutorial/

### Who do I talk to? ###

* Repo owner or admin ** Vadim Klochko **

* Other community or team contact