package mobile.com.headsup.utils;

import java.util.List;

import mobile.com.headsup.device.Cacheable;
import mobile.com.headsup.device.Device;

public class DeviceUtils {

	public static Cacheable remove(final List<Cacheable> devices, final String imei) {
		Cacheable myDevice = null;
		if (devices != null) {
			for (int i = 0; i < devices.size(); i++) {
				myDevice = devices.get(i);
				if (myDevice.getId().equalsIgnoreCase(imei)) {
					devices.remove(i);
					break;
				}
			}
		}

//		if (devices != null) {
//			for (Device device : devices) {
//				if (device.getId().equalsIgnoreCase(imei)) {
//					myDevice = device;
//					break;
//				}
//			}
//		}
//		devices.remove(myDevice);
		return myDevice;
	}
}
