package mobile.com.headsup.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

//import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.Polygon;
//import com.google.android.gms.maps.model.PolygonOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.Polygon;
import com.androidmapsextensions.PolygonOptions;
import com.androidmapsextensions.Polyline;
import com.androidmapsextensions.PolylineOptions;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

import mobile.com.headsup.cluster.ClusterItem;
import mobile.com.headsup.device.Cacheable;
import mobile.com.headsup.device.Device;

public class MapUtils {

	public static final double M = 1000;
	public static final int DEFAULT_ZOOM = 8;
	public static Map<Integer, Area> zoomMap = new TreeMap<Integer, Area>();

	//private ConcurrentSkipListMap<Integer, Area> map = new ConcurrentSkipListMap<Integer, Area>();

	private static class Area {
		private double area;
		private double width;
		private double height;
		public Area(final double width, final double height) {
			this.width = width;
			this.height = height;
			this.area = width * height;
		}
	};

	static {
		zoomMap.put(1, new Area(5000000d, 5000000d));
		zoomMap.put(2, new Area(1250000d, 625000d));
		zoomMap.put(3, new Area(156000d, 156000d));
		zoomMap.put(4, new Area(39100d, 19500d));
		zoomMap.put(5, new Area(4890d, 4890d));
		zoomMap.put(6, new Area(1220d, 610d));
		zoomMap.put(7, new Area(153d, 153d));
		zoomMap.put(8, new Area(38.2d, 19.1d));
		zoomMap.put(9, new Area(4.77d, 4.77d));
		zoomMap.put(10, new Area(1.19d, 0.596d));
		zoomMap.put(11, new Area(0.149d, 0.149d));
		zoomMap.put(11, new Area(0.037d, 0.0186d));
	}

	public static void updateMarkersIconColor(final List<Marker> markers, final float hue) {
		if (markers != null) {
			for (Marker marker : markers) {
				MapUtils.updateMarkerIconColor(marker, hue);
			}
		}
	}

	public static void updateMarkerIconColor(final Marker marker, final float hue) {
		if (marker != null) {
			marker.setIcon(BitmapDescriptorFactory.defaultMarker(hue));
		}
	}

	public static Marker removeMarker(final GoogleMap map, final SortedMap<String, Marker> markers, final Device device) {
		final Marker marker = markers.remove(device.getId());
		if (marker != null) {
			marker.remove();
		}
		return marker;
	}

	public static Marker addClusterItem(final Context context, final GoogleMap map, final ClusterItem item) {
		final Marker marker = map.addMarker(new MarkerOptions().position(item.getPosition()));
		marker.setIcon(BitmapDescriptorFactory.fromBitmap(item.getIcon(context)));
		marker.setDraggable(false);
		return marker;
	}

	public static Marker addMarker(final Context context, final GoogleMap map, final SortedMap<String, Marker> markers, final Cacheable device) {
		final Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(device.getLatitude(), device.getLongitude())));
		marker.setIcon(BitmapDescriptorFactory.defaultMarker(getMarkerColor(context, device.getPhoneNumber())));
		//marker.setSnippet(device.getPhoneNumber());
		marker.setData(device);
		marker.setDraggable(false);
		marker.setTitle(MapUtils.getContactName(context, device.getPhoneNumber()));
        if (!StringUtils.isEmpty(marker.getTitle())) {
            marker.showInfoWindow();
        }
		markers.put(device.getId(), marker);
		return marker;
	}

	public static float getMarkerColor(final Context context, final String phoneNumber) {
		if (isContactExists(context, phoneNumber)) {
			return BitmapDescriptorFactory.HUE_ORANGE;
		} else {
			return BitmapDescriptorFactory.HUE_GREEN;
		}
	}

	public static String getContactName(final Context context, final String phoneNumber) {
		String name = null;
		Cursor cursor = null;
		try {
			final ContentResolver resolver = context.getContentResolver();
			final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
			cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

			if (cursor != null) { // cursor not null means number is found contactsTable
				if (cursor.moveToFirst()) {   // so now find the contact Name
					name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				}
			}
		} catch (Exception e) {
            Log.e("MapUtils", e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return name;
	}

	public static boolean isContactExists(final Context context, final String phoneNumber) {
		boolean isContactExists = false;
		Cursor cursor = null;
		try {
			final ContentResolver resolver = context.getContentResolver();
			final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
			cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
            if (cursor != null) { // cursor not null means number is found contactsTable
                isContactExists = cursor.moveToFirst();
            }
		} catch (Exception e) {
            Log.e("MapUtils", e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return isContactExists;
	}

	public static List<Marker> getMarkers(final List<Marker> markers, final List<LatLng> positions) {
		final List<Marker> list = new ArrayList<Marker>();
		if (markers != null) {
			for (Marker marker : markers) {
				if (positions.contains(marker.getPosition())) {
					list.add(marker);
				}
			}
		}
		return list;
	}

	public static List<LatLng> getPositions(final List<Marker> markers) {
		final List<LatLng> points = new ArrayList<LatLng>();
		if (markers != null) {
			for (Marker marker : markers) {
				if (!points.contains(marker.getPosition())) {
					points.add(marker.getPosition());
				}
			}
		}
		return points;
	}
	public static List<Marker> getWithin(final List<LatLng> positions, final SortedMap<String, Marker> markers) {
		List<Marker> bounderMarkers = new ArrayList<Marker>();
		if (positions != null && !positions.isEmpty()) {
			final LatLngBounds bounds = MapUtils.build(positions);
			bounderMarkers = MapUtils.getWithin(bounds, new ArrayList<Marker>(markers.values()));
		}
		return bounderMarkers;
	}

	public static List<Marker> getWithin(final LatLngBounds bounds, final List<Marker> markers) {
		final List<Marker> out = new ArrayList<Marker>();
		if (markers != null) {
			for (Marker marker : markers) {
				if (bounds.contains(marker.getPosition())) {
					out.add(marker);
				}
			}
		}
		return out;
	}

	public static List<String> getDeviceIds(final List<Marker> markers) {
		final List<String> out = new ArrayList<String>();
		if (markers != null) {
			for (Marker marker : markers) {
				final Cacheable device = (Cacheable) marker.getData();
				out.add(device.getId());
			}
		}
		return out;
	}

	public static LatLngBounds build(final List<LatLng> points) {
		LatLngBounds.Builder bounds = LatLngBounds.builder();
		if (points != null) {
			for (LatLng point : points) {
				bounds.include(point);
			}
		}
		return bounds.build();
	}

	public static void clearPolygonOptions(final PolygonOptions options) {
		if (options != null) {
			options.getPoints().clear();
		}
	}

	public static void clearPolylineOptions(final PolylineOptions options) {
		if (options != null) {
			options.getPoints().clear();
		}
	}

	public static void clearPolygons(final List<Polygon> polygons) {
		if (polygons != null) {
			for (Polygon polygon : polygons) {
				clearPolygon(polygon);
			}
			polygons.clear();
		}
	}

	public static void clearPolylines(final List<Polyline> polylines) {
		if (polylines != null) {
			for (Polyline polyline : polylines) {
				clearPolyline(polyline);
			}
			polylines.clear();
		}
	}

	public static void clearPolygon(final Polygon polygon) {
		if (polygon != null) {
			polygon.remove();
		}
	}

	public static void clearPolyline(final Polyline polyline) {
		if (polyline != null) {
			polyline.remove();
		}
	}

	public static int zoom(final double targetArea, final double precision) {
		int zoom = DEFAULT_ZOOM;
		final double factor = precision / M;
		for (final Integer key : zoomMap.keySet()) {
			final Area area = zoomMap.get(key);
			//Log.d("MapsUtils", "key: " + key + " area: " + area);
			final double zoomArea = factor * area.width * factor * area.height;
			//Log.d("MapsUtils", "zoomArea: " + zoomArea);
			if (targetArea >= zoomArea) {
				zoom = key;
				//Log.d("MapsUtils", "targetArea >= zoomArea " + targetArea + ">=" + zoomArea);
				break;
			}
		}
		return zoom;
	}

	public static double area(final double south, final double west, final double north, final double east, final double precision) {
		Log.d("MapsUtils", "south: " + south + " west: " + west + " north: " + north + " east: " + east);
		final double width = distance(south, west, south, east, precision);
		final double height = distance(south, west, north, west, precision);
		Log.d("MapsUtils", "width(m): " + width);
		Log.d("MapsUtils", "height(m): " + height);
		return width * height;
	}

	public static double distance(final double latitude1, final double longitude1, final double latitude2, final double longitude2, final double precision) {
		return precision * 6371d * Math.acos( Math.sin(Math.toRadians(latitude1))*Math.sin(Math.toRadians(latitude2)) + Math.cos(Math.toRadians(latitude1))*Math.cos(Math.toRadians(latitude2)) * Math.cos(Math.toRadians(longitude2 - longitude1)) );
	}
}
