package mobile.com.headsup.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class ByteUtils {

	public static boolean isEmpty(final byte[] arr) {
		return arr == null || arr.length == 0;
	}

	public static byte[] collateMessages(HashMap<String, HashMap<String, byte[]>> partialMessages) {
		byte[] combined = new byte[0];
		for (int i = 0; i < partialMessages.size(); i++) {
			combined = ByteUtils.concat(combined, ByteUtils.collateFrames(partialMessages.get(String.valueOf(i + 1))));
		}
		return combined;
	}

	public static byte[] collateFrames(HashMap<String, byte[]> partialFrames) {
		byte[] combined = new byte[0];
		for (int i = 0; i < partialFrames.size(); i++) {
			combined = ByteUtils.concat(combined, partialFrames.get(String.valueOf(i + 1)));
		}
		return combined;
	}

	public static byte[] concat(final byte[] arr1, final byte[] arr2) {
		byte[] combined = new byte[arr1.length + arr2.length];
		System.arraycopy(arr1, 0, combined, 0, arr1.length);
		System.arraycopy(arr2, 0, combined, arr1.length, arr2.length);
		return combined;
	}

	public static int computeTotalSizeOfMessages(HashMap<String, HashMap<String, byte[]>> partialMessages) {
		int length = 0;
		final Iterator<String> iterator = partialMessages.keySet().iterator();
		while (iterator.hasNext()) {
			length += ByteUtils.computeTotalSizeOfFrames(partialMessages.get(iterator.next()));
		}
		return length;
	}

	public static int computeTotalSizeOfFrames(HashMap<String, byte[]> partialFrames) {
		int length = 0;
		final Iterator<String> iterator = partialFrames.keySet().iterator();
		while (iterator.hasNext()) {
			length += partialFrames.get(iterator.next()).length;
		}
		return length;
	}

	public static String convertToString(final byte[] arr) {
		try {
			return new String(arr);//, "UTF-8"
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		/*
		catch (UnsupportedEncodingException uee) {
			return null;
		}
		*/
	}
}
