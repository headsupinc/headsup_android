package mobile.com.headsup.utils;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

	public static boolean contains(final File[] files, final String str) {
		boolean out = false;
		for (int i = files.length - 1; i >=0; i--) {
			File file = files[i];
			if (file.getPath().contains(str)) {
				out = true;
				break;
			}
		}
		return out;
	}
	public static String getPath(final File[] files, final String str) {
		String path = StringUtils.EMPTY;
		for (int i = files.length - 1; i >=0; i--) {
			File file = files[i];
			if (file.getPath().contains(str)) {
				path = file.getPath();
				break;
			}
		}
		return path;
	}

	public static void deleteFiles(final File dir, final List<String> filesPaths) {
		final List<File> files = Arrays.asList(dir.listFiles());
		for (String filePath : filesPaths) {
			int index = files.indexOf(new File(filePath));
			if (index >= 0) {
				files.get(index).delete();
			}
		}
	}
	/*
	private static byte[] readBytes(final File file) {
		FileInputStream fileInputStream = null;
		byte[] bytes = new byte[(int) file.length()];
		try
		{
			//convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bytes);
			fileInputStream.close();
			for (int i = 0; i < bytes.length; i++)
			{
				System.out.print((char) bFile[i]);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return bytes;
	}
	*/
	public static byte[] readBytes(final File file) {
		byte[] bytes = new byte[(int) file.length()];
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			//byte[] targetArray = new byte[inputStream.available()];
			inputStream.read(bytes);
			inputStream.close();
		} catch (Exception e) {
			Log.e("FileUtils", e.getMessage(), e);
		} finally {
			return bytes;
		}
	}
}
