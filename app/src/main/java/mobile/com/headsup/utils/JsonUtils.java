package mobile.com.headsup.utils;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

	public static Object readValue(final String json, final Class clazz) {
		Object object = null;
		final ObjectMapper mapper = new ObjectMapper();
		try {
			if (!StringUtils.isEmpty(json) && clazz != null) {
				object = mapper.readValue(json, clazz);
			}
		} catch (Exception e) {
			Log.e("JsonUtils", e.getMessage(), e);
		} finally {
			return object;
		}
	}

	public static String writeValue(final Object object) {
		String json = StringUtils.EMPTY;
		final ObjectMapper mapper = new ObjectMapper();
		try {
			if (object != null) {
				json = mapper.writeValueAsString(object);
			}
		} catch (Exception e) {
			Log.e("JsonUtils", e.getMessage(), e);
		} finally {
			return json;
		}
	}
}
