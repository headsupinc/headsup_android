package mobile.com.headsup.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mobile.com.headsup.R;

public class MediaUtils {

	// Activity request codes
	public static final int SELECT_PICTURE_ACTIVITY_REQUEST_CODE = 0;
	public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	public static boolean isImage(final Resources resources, final String path) {
		return path.contains(SystemUtils.getResourceString(resources, R.string.image_filename_format_prefix));
	}

	public static boolean isVideo(final Resources resources, final String path) {
		return path.contains(SystemUtils.getResourceString(resources, R.string.video_filename_format_prefix));
	}

	public static byte[] getImageBytes(final Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		return stream.toByteArray();

//		ByteBuffer buffer = ByteBuffer.allocate(bitmap.getByteCount());
//		bitmap.copyPixelsToBuffer(buffer);
//		return buffer.array();
	}

	public static Bitmap getImage(final byte[] bytes) {
		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	}
	public static Bitmap getImage(final byte[] bytes, final BitmapFactory.Options options) {
		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
	}

	public static Bitmap cropToSquare(Bitmap bitmap) {
		int width  = bitmap.getWidth();
		int height = bitmap.getHeight();
		int newWidth = (height > width) ? width : height;
		int newHeight = (height > width)? width : height;
		int cropW = (width - height) / 2;
		cropW = (cropW < 0)? 0: cropW;
		int cropH = (height - width) / 2;
		cropH = (cropH < 0)? 0: cropH;
		return Bitmap.createBitmap(bitmap, cropW, cropH, newWidth, newHeight);
	}

	public static Bitmap loadBitmap(final Context context, final String fileName){
		Bitmap bitmap = null;
		FileInputStream fis;
		try {
			fis = context.openFileInput(fileName);
			bitmap = BitmapFactory.decodeStream(fis);
			fis.close();
		} catch (Exception e) {
			Log.e("MediaUtils", e.getMessage(), e);
		}
		return bitmap;
	}

	public static void save(final Context context, final String filename, final byte[] bytes) {
		FileOutputStream outputStream;

		try {
			//File file = new File(path);
			outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
			outputStream.write(bytes);
			outputStream.close();
		} catch (Exception e) {
			Log.e("MediaUtils", e.getMessage(), e);
		}
	}

	/**
	 * Check if this device has a camera
	 */
	public static boolean isDeviceSupportsCamera(Context context) {
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}
	/**
	 * Creating file uri to store image/video
	 */
	public static Uri getOutputMediaFileUri(final Resources resources, final int type, final String directory) {
		return Uri.fromFile(getOutputMediaFile(resources, type, directory));
	}

	/**
	 * returning image / video
	 */
	private static File getOutputMediaFile(final Resources resources, final int type, final String directory) {

		// External sdcard location
		final File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), directory);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				//Log.d(directory, "Could not create " + directory + " directory");
				return null;
			}
		}

		// Create a media file name
		final String timeStamp = new SimpleDateFormat(SystemUtils.getResourceString(resources, R.string.captured_media_timestamp_format), Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator + String.format(SystemUtils.getResourceString(resources, R.string.image_filename_format), timeStamp.hashCode()));
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator + String.format(SystemUtils.getResourceString(resources, R.string.video_filename_format), timeStamp.hashCode()));
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int getHashFromImageFilename(final Resources resources, final String fileName) {
		final String from = SystemUtils.getResourceString(resources, R.string.filename_format_delimiter);
		final String to = SystemUtils.getResourceString(resources, R.string.image_filename_format_extension);
		final String hashcode = fileName.substring(fileName.lastIndexOf(from) + from.length(), fileName.lastIndexOf(to));
		return Integer.valueOf(hashcode);
	}

	public static int getHashFromVideoFilename(final Resources resources, final String fileName) {
		final String from = SystemUtils.getResourceString(resources, R.string.filename_format_delimiter);
		final String to = SystemUtils.getResourceString(resources, R.string.video_filename_format_extension);
		final String hashcode = fileName.substring(fileName.lastIndexOf(from) + from.length(), fileName.lastIndexOf(to));
		return Integer.valueOf(hashcode);
	}
}
