package mobile.com.headsup.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.com.headsup.R;
import mobile.com.headsup.enums.MimeTypes;

public class SystemUtils {

	public static String getResourceString(final Resources resources, int id) {
		return resources.getText(id).toString();
	}

	public static int getResourceColor(final Resources resources, int id) {
		return resources.getColor(id);
	}

	public static List<String> getResourcesString(final Resources resources, int[] ids) {
		final List<String> list = new ArrayList<String>();
		for (int id : ids) {
			list.add(SystemUtils.getResourceString(resources, id));
		}
		return list;
	}

	public static List<Integer> getResourcesColor(final Resources resources, int[] ids) {
		final List<Integer> list = new ArrayList<Integer>();
		for (int id : ids) {
			list.add(SystemUtils.getResourceColor(resources, id));
		}
		return list;
	}

	public static void hideKeyboard(final Context context, final View view, int field) {
		if (context != null && view != null) {
			((InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), field);
			((InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromInputMethod(view.getWindowToken(), field);
		}
	}

	public static void showKeyboard(final Context context, final View view, int field) {
		if (context != null && view != null) {
			((InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE)).showSoftInput(view, field);
		}
	}
}
