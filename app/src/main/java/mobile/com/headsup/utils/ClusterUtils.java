package mobile.com.headsup.utils;

import android.content.res.Resources;

import java.util.List;

import mobile.com.headsup.R;
import mobile.com.headsup.cluster.Cluster;

/**
 * Created by Vadim on 11/12/2015.
 */
public class ClusterUtils {

    public static int getTotalSize(final List<Cluster> clusters) {
        int total = 0;
        for (Cluster cluster : clusters) {
            total += cluster.getSize();
        }
        return total;
    }

    public static int getClusterSize(final Resources resources, final int count) {
        if (count < 1000) {
            return resources.getDimensionPixelSize(R.dimen.map_cluster_size_small);
        } else if (count < 10000) {
            return resources.getDimensionPixelSize(R.dimen.map_cluster_size_medium);
        } else {
            return resources.getDimensionPixelSize(R.dimen.map_cluster_size_large);
        }
    }

    public static int getClusterColor(final Resources resources, final int count) {
        if (count < 1000) {
            return resources.getColor(R.color.color_map_cluster_small);
        } else if (count < 10000) {
            return resources.getColor(R.color.color_map_cluster_medium);
        } else {
            return resources.getColor(R.color.color_map_cluster_large);
        }
    }

    public static String getClusterText(final Resources resources, final int count) {
        if (count <= 100) {
            return String.valueOf(count);
        } else if (count <= 1000) {
            return String.valueOf((count / 100) * 100) + "+";
        } else if (count <= 10000) {
            return String.valueOf((count / 1000) * 1000) + "+";
        } else if (count <= 100000) {
            return String.valueOf((count / 10000) * 10000) + "K+";
        } else if (count <= 1000000) {
            return String.valueOf((count / 100000) * 100000) + "K+";
        } else if (count <= 10000000) {
            return String.valueOf((count / 1000000) * 1000000) + "M+";
        } else if (count <= 100000000) {
            return String.valueOf((count / 10000000) * 10000000) + "M+";
        } else if (count <= 1000000000) {
            return String.valueOf((count / 100000000) * 100000000) + "B+";
        } else {
            return String.valueOf((count / 1000000000) * 1000000000) + "B+";
        }
    }

    public static float getClusterTextSize(final Resources resources, final int count) {
        if (count <= 100) {
            return 18f;
        } else if (count <= 1000) {
            return 18f;
        } else if (count <= 10000) {
            return 18f;
        } else if (count <= 100000) {
            return 22f;
        } else if (count <= 1000000) {
            return 22f;
        } else if (count <= 10000000) {
            return 22f;
        } else if (count <= 100000000) {
            return 26f;
        } else if (count <= 1000000000) {
            return 26f;
        } else {
            return 26f;
        }
    }
}
