package mobile.com.headsup.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.springframework.util.Base64Utils;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import mobile.com.headsup.R;
import mobile.com.headsup.async.MessageHandlerTask;
import mobile.com.headsup.enums.MimeTypes;

public class ActivityUtils {

	public static View appendMessage(final LinearLayout parent, View child, int resourceId, MimeTypes type, byte[] payload) {
		View view = null;
		LinearLayout linearLayout = (LinearLayout) child;
		if (MimeTypes.OCTET_STREAM.equals(type)) {
			// bitmap factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image to avoid OutOfMemory Exception for larger images
			//options.inSampleSize = 8;
			Bitmap imageThumb = MediaUtils.getImage(payload, options);

			ImageView imageView = (ImageView) linearLayout.findViewById(resourceId);
			imageView.setId(imageView.getId() + parent.getChildCount());
			imageView.setImageBitmap(imageThumb);
			view = imageView;
		} else if (MimeTypes.TEXT_PLAIN.equals(type)){
			TextView textView = (TextView) linearLayout.findViewById(resourceId);
			textView.setId(textView.getId() + parent.getChildCount());
			textView.setText(new String(payload));
			view = textView;
		}
		parent.addView(linearLayout, parent.getChildCount());
		((ScrollView) parent.getParent()).fullScroll(View.FOCUS_DOWN);
		return view;
	}

	public static View appendImage(final LinearLayout parent, final View child, final int resourceId, final Bitmap bitmap) {
		LinearLayout linearLayout = (LinearLayout) child;
		ImageView imageView = (ImageView) linearLayout.findViewById(resourceId);
		imageView.setId(imageView.getId() + parent.getChildCount());
		imageView.setImageBitmap(bitmap);
		parent.addView(linearLayout, parent.getChildCount());
		((ScrollView) parent.getParent()).fullScroll(View.FOCUS_DOWN);
		return imageView;
	}

	public static Bitmap getCoverImage(final Resources resources, final String mediaType, final String path) {
		Bitmap bitmap = null;
		if (SystemUtils.getResourceString(resources, R.string.label_media_type_video).equals(mediaType)) {
			//TODO: need to get the same aspect ratio as all other images
			bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
		}
		return bitmap;
	}

	public static void setTextColor(TextView textView, Map<String, Integer> colorMap, MimeTypes type, String id) {
		if (MimeTypes.TEXT_PLAIN.equals(type)){
			textView.setTextColor(colorMap.get(id));
		}
	}

	public static void setImageViewBackgroundColor(ImageView imageView, Map<String, Integer> colorMap, MimeTypes type, String id) {
		if (MimeTypes.OCTET_STREAM.equals(type)){
			imageView.setBackgroundColor(colorMap.get(id));
		}
	}

	public static void appendMessage(View child, MimeTypes type, byte[] payload) {
		if (MimeTypes.OCTET_STREAM.equals(type)) {
			//Bitmap bitmap = BitmapFactory.decodeByteArray(payload, 0, payload.length);
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image to avoid OutOfMemory Exception for larger images
			//options.inSampleSize = 8;
			Bitmap bitmap = MediaUtils.getImage(payload, options);
			ImageView imageView = (ImageView) child;
			imageView.setImageBitmap(bitmap);
		} else if (MimeTypes.TEXT_PLAIN.equals(type)){
			TextView textView = (TextView) child;
			textView.setText(new String(payload));
		}
	}

	public static void add(final String value, final List<String> list) {
		if (value != null && !value.trim().isEmpty()) {
			if (list != null && !list.contains(value)) {
				list.add(value);
			}
		}
	}

	public static void add(final String key, final Map<String, Integer> map, final List<Integer> colors) {
		if (key != null && !key.trim().isEmpty()) {
			if (map != null && !map.containsKey(key)) {
				Random random = new SecureRandom();
				map.put(key, Color.rgb(
						random.nextInt(colors.get(0)),
						random.nextInt(colors.get(1)),
						random.nextInt(colors.get(2))));
			}
		}
	}

	public static Intent startActivityInsertContact(final String phoneNumber) {
		// Creates a new Intent to insert a contact
		final Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
		// Sets the MIME type to match the Contacts Provider
		intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
		intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);
		intent.putExtra("finishActivityOnSaveCompleted", true);
		return intent;
	}

	/*
        public static Intent startActivityInsertContact(final String phoneNumber) {
            // Creates a new Intent to insert a contact
            final Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);
            // Sets the MIME type to match the Contacts Provider
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);
            intent.putExtra("finishActivityOnSaveCompleted", true);
            return intent;
        }
	*/
/*
        public static Intent startActivityInsertContact(final String phoneNumber) {
            // Creates a new Intent to insert a contact
            final Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);

            // Sets the MIME type to match the Contacts Provider
            intent.putExtra("finishActivityOnSaveCompleted", true);
            return intent;
        }
*/
	/*
	public static Intent startActivityInsertContact(final Context context, final String phoneNumber) {
		// Creates a new Intent to insert a contact
		final Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
		// Sets the MIME type to match the Contacts Provider
		//intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
		final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
		final ContentResolver resolver = context.getContentResolver();
		intent.setDataAndType(ContactsContract.Contacts.getLookupUri(resolver, uri), ContactsContract.Contacts.CONTENT_ITEM_TYPE);
		intent.putExtra("finishActivityOnSaveCompleted", true);
		return intent;
	}
	*/

	public static Intent startActivityEditContact(final Context context, final String phoneNumber) {
		// Creates a new Intent to insert a contact
		final Intent intent = new Intent(Intent.ACTION_EDIT);
		// Sets the MIME type to match the Contacts Provider
		//intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
		final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
		final ContentResolver resolver = context.getContentResolver();
		intent.setDataAndType(ContactsContract.Contacts.getLookupUri(resolver, uri), ContactsContract.Contacts.CONTENT_ITEM_TYPE);
		intent.putExtra("finishActivityOnSaveCompleted", true);
		return intent;
	}

	public static void sendContactRequest(final MessageHandlerTask task, final Resources resources, final String sessionId, final String sourceId, final String sourcePhoneNumber, final String deviceId, final int resourceId) {
		try {
			if (!StringUtils.isEmpty(sourceId) && !StringUtils.isEmpty(sourcePhoneNumber)) {
				final byte[] payload = sourcePhoneNumber.getBytes();
				final byte[] encodedPayload = Base64Utils.encode(payload);
				final Map<String, String> headers = new HashMap<>();
				headers.put("messageId", String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf(payload.hashCode()));
				headers.put("messengerId", sourceId);
				headers.put("sessionId", sessionId);
				headers.put("content-type", MimeTypes.TEXT_PLAIN.getValue());
				headers.put("media-type", SystemUtils.getResourceString(resources, resourceId));
				//headers.put("ACK", "client");//client-individual
				task.getMessageHandler().sendMessage(null, String.format(SystemUtils.getResourceString(resources, R.string.textStreamChannel), deviceId),
						headers,
						encodedPayload);
			}
		} catch (Exception e) {
			Log.e("ActivityUtils", e.getMessage(), e);
		}
	}
}
