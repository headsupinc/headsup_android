package mobile.com.headsup.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

	public static final String EMPTY = "";
	public static boolean isEmpty(final String str) {
		return str == null || str.isEmpty();
	}
}
