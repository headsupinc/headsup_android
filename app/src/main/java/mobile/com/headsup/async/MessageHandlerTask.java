package mobile.com.headsup.async;

import mobile.com.headsup.stompws.Stomp;

/**
 * Created by Vadim on 1/24/2016.
 */
public interface MessageHandlerTask {
    public Stomp getMessageHandler();
}
