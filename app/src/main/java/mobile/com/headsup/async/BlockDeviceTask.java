package mobile.com.headsup.async;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import mobile.com.headsup.R;

/**
 * Created by Vadim on 10/22/2015.
 */
public class BlockDeviceTask extends AsyncTask<Object, Void, Void> {
    private Resources resources;

    public BlockDeviceTask(final Resources resources) {
        this.resources = resources;
    }
    @Override
    protected Void doInBackground(final Object... params) {
        try {
            final String url = resources.getText(R.string.webserviceHostname).toString() + resources.getText(R.string.webserviceEndpointBlockDevice).toString();
            final RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.postForLocation(url, null, (Object[]) params);
            return null;
        } catch (Exception e) {
            Log.e("BlockDeviceTask", e.getMessage(), e);
        }
        return null;
    }
}

