package mobile.com.headsup.async;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import mobile.com.headsup.R;
import mobile.com.headsup.device.Device;

/**
 * Created by Vadim on 10/22/2015.
 */
public class UpdateDeviceTask extends AsyncTask<Object, Void, Void> {
    private Resources resources;

    public UpdateDeviceTask(final Resources resources) {
        this.resources = resources;
    }
    @Override
    protected Void doInBackground(final Object... params) {
        try {
            final String url = resources.getText(R.string.webserviceHostname).toString() + resources.getText(R.string.webserviceEndpointUpdateDevice).toString();
            final RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForEntity(url, null, (Object[]) params);
            return null;
        } catch (Exception e) {
            Log.e("UpdateDeviceTask", e.getMessage(), e);
        }
        return null;
    }
    /*
    private class UpdateTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(final String... params) {
            try {
                final String url = getResources().getText(R.string.webserviceHostname).toString() + getResources().getText(R.string.webserviceEndpointUpdateDevice).toString();
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                final Device[] devices = restTemplate.getForObject(url, Device[].class, (Object[]) params);

            } catch (Exception e) {
                Log.e("MapsActivity", e.getMessage(), e);
            }
            return null;
        }
    }
     */
}

