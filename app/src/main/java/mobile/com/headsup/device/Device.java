package mobile.com.headsup.device;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.com.headsup.enums.DeviceStatus;

public class Device implements Cacheable {

    private String phoneNumber;

    private String id;

    private float latitude;

    private float longitude;

    private DeviceStatus deviceStatus;

    private List<String> blocked;

    public String getPhoneNumber() { return phoneNumber; }

    public String getId() { return id; }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public DeviceStatus getDeviceStatus() { return deviceStatus; }

    public List<String> getBlocked() { return blocked; }
}
