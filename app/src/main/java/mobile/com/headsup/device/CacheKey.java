package mobile.com.headsup.device;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.com.headsup.enums.DeviceStatus;

/**
 * Created by Vadim on 11/7/2015.
 */
public class CacheKey implements Comparable<CacheKey>, Cacheable {

    private String id;
    private String phoneNumber;
    private float latitude;
    private float longitude;
    private DeviceStatus deviceStatus;
    private List<String> blocked;

    public String getId() { return id; }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public float getLatitude() { return latitude; }

    public float getLongitude() { return longitude; }

    public DeviceStatus getDeviceStatus() { return deviceStatus; }

    public List<String> getBlocked() {
        return blocked;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof CacheKey)) {
            return false;
        }
        final CacheKey other = (CacheKey) object;
        return getId().equalsIgnoreCase(other.getId());
    }

    @Override
    public int compareTo(final CacheKey other) {
        if (this == other) {
            return 0;
        }
        if (other == null) {
            return 0;
        }
        return getId().compareToIgnoreCase(other.getId());
    }
}
