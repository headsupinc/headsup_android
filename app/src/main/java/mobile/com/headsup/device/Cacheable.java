package mobile.com.headsup.device;

import java.util.List;
import java.util.Map;

import mobile.com.headsup.enums.DeviceStatus;

/**
 * Created by Vadim on 11/20/2015.
 */
public interface Cacheable {

    public String getId();

    public String getPhoneNumber();

    public float getLatitude();

    public float getLongitude();

    public DeviceStatus getDeviceStatus();

    public List<String> getBlocked();
}
