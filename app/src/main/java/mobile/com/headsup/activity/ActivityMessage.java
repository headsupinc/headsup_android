package mobile.com.headsup.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.springframework.util.Base64Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mobile.com.headsup.R;
import mobile.com.headsup.adapter.StableArrayAdapter;
import mobile.com.headsup.animation.ProgressBarAnimation;
import mobile.com.headsup.async.BlockDeviceTask;
import mobile.com.headsup.async.MessageHandlerTask;
import mobile.com.headsup.enums.MimeTypes;
import mobile.com.headsup.stompws.ListenerProgressUpdate;
import mobile.com.headsup.stompws.ListenerSubscription;
import mobile.com.headsup.stompws.ListenerWSNetwork;
import mobile.com.headsup.stompws.Stomp;
import mobile.com.headsup.stompws.Subscription;
import mobile.com.headsup.utils.ActivityUtils;
import mobile.com.headsup.utils.ByteUtils;
import mobile.com.headsup.utils.FileUtils;
import mobile.com.headsup.utils.MapUtils;
import mobile.com.headsup.utils.MediaUtils;
import mobile.com.headsup.utils.StringUtils;
import mobile.com.headsup.utils.SystemUtils;

/**
 * Created by Vadim on 9/3/2015.
 */
public class ActivityMessage extends FragmentActivity implements AdapterView.OnItemClickListener, View.OnClickListener, View.OnLongClickListener, EditText.OnEditorActionListener, ListenerProgressUpdate, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    private byte[] message;
    private String[] deviceIds;
    private List<String> sessionIds;
    private Map<String, Integer> colorMap;
    private Map<String, ProgressBar> progressTasks;
    private String messengerId;
    private String messengerPhone;
    private String sessionId;
    private String deviceId;
    private String devicePhone;
    private String contentType;
    private String mediaType;
    private String contentDescription;
    private StompWsTask taskText;
    private StompWsTask taskImage;
    private StompWsTask taskFile;
    //private EditText editText;
    private EmojiconEditText editText;
    private List<String> storedMedia;

    private Uri fileUri; // file url to store image/video

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        //editText = (EditText) findViewById(R.id.editText);
        editText = (EmojiconEditText) findViewById(R.id.editText);
        final StableArrayAdapter adapterEditActions = new StableArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                SystemUtils.getResourcesString(getResources(), new int[]{R.string.label_MediaLibrary, R.string.label_TakePhoto, R.string.label_RecordVideo, R.string.label_Cancel}));

        final ListView listViewEditActions = (ListView) findViewById(R.id.listViewEditActions);
        listViewEditActions.setAdapter(adapterEditActions);
        listViewEditActions.setOnItemClickListener(this);

//        final StableArrayAdapter adapterUserActions = new StableArrayAdapter(
//                this,
//                android.R.layout.simple_list_item_1,
//                SystemUtils.getResourcesString(getResources(), new int[]{R.string.label_BlockSender, R.string.label_Cancel}));
//
//        final ListView listViewUserActions = (ListView) findViewById(R.id.listViewUserActions);
//        listViewUserActions.setAdapter(adapterUserActions);
//        listViewUserActions.setOnItemClickListener(this);

        // receive the arguments from the previous Activity
        final Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        deviceId = tm.getDeviceId();
        devicePhone = tm.getLine1Number();
        sessionIds = new ArrayList<>();
        colorMap = new TreeMap<>();
        progressTasks = new TreeMap<>();
        storedMedia = new ArrayList<>();

        // assign the values to string-arguments
        deviceIds = extras.getStringArray("deviceIds");
        messengerId = extras.getString("messengerId");
        messengerPhone = extras.getString("messengerPhone");
        sessionId = extras.getString("sessionId");
        contentType = extras.getString("contentType");
        mediaType = extras.getString("mediaType");
        message = extras.getByteArray("message");
        final String recipientId = extras.getString("recipientId");

        if (!ByteUtils.isEmpty(message) && !StringUtils.isEmpty(contentType) && !StringUtils.isEmpty(recipientId) && !StringUtils.isEmpty(messengerId) && !StringUtils.isEmpty(messengerPhone)) {
            ActivityUtils.add(recipientId, sessionIds);
            ActivityUtils.add(recipientId, colorMap, SystemUtils.getResourcesColor(getResources(), new int[]{R.integer.color_map_threshold_r, R.integer.color_map_threshold_g, R.integer.color_map_threshold_b}));

            final MimeTypes type = MimeTypes.fromValue(contentType);
            processMessage(type, mediaType, recipientId, messengerId, messengerPhone, message);
        }

        editText.setOnEditorActionListener(this);

        taskText = new StompWsTask();
        taskText.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.textStreamChannel), sessionId));

        taskImage = new StompWsTask();
        taskImage.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.imageStreamChannel), sessionId));

        taskFile = new StompWsTask();
        taskFile.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.fileStreamChannel), sessionId));
    }

    protected void onDestroy() {
        super.onDestroy();
        FileUtils.deleteFiles(getApplicationContext().getFilesDir(), storedMedia);
    }

    private void resetView() {
        findViewById(R.id.editorPanel).setVisibility(View.VISIBLE);
        findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
        findViewById(R.id.listViewPanelEditActions).setVisibility(View.GONE);
        findViewById(R.id.emojiViewPanel).setVisibility(View.GONE);
        SystemUtils.showKeyboard(this.getApplicationContext(), editText, InputMethodManager.SHOW_IMPLICIT);
        ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
    }
    public void setContentView(View view) {
        if (view.getId() == R.id.imageButtonLeft || view.getId() == R.id.imageButtonRight) {
            if (view.getId() == R.id.imageButtonLeft) {
                SystemUtils.hideKeyboard(this.getApplicationContext(), editText, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
                findViewById(R.id.editorPanel).setVisibility(View.GONE);
                findViewById(R.id.emojiViewPanel).setVisibility(View.GONE);
                findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
                findViewById(R.id.listViewPanelEditActions).setVisibility(View.VISIBLE);
            } else if (view.getId() == R.id.imageButtonRight) {
                final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.emojiViewPanel);
                if (relativeLayout.getVisibility() == View.VISIBLE) {
                    SystemUtils.showKeyboard(this.getApplicationContext(), editText, InputMethodManager.SHOW_IMPLICIT);
                    relativeLayout.setVisibility(View.GONE);
                } else {
                    SystemUtils.hideKeyboard(this.getApplicationContext(), editText, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
                    relativeLayout.setVisibility(View.VISIBLE);
                }
                findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
                findViewById(R.id.listViewPanelEditActions).setVisibility(View.GONE);
            }
            ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);

            //ListView listViewEditActions = (ListView) findViewById(R.id.listViewEditActions);
            //listViewEditActions.setVisibility(View.VISIBLE);
            /*
            listViewPanelEditActions.animate().setDuration(2000).alpha(0)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        //list.remove(item);
                        //adapter.notifyDataSetChanged();
                        view.setAlpha(1);
                        view.setVisibility(View.VISIBLE);
                    }
                });
                */

        } else {
            File dir = getApplicationContext().getFilesDir();
            String filePath = FileUtils.getPath(dir.listFiles(), String.valueOf(view.getId()));
            Intent intent = new Intent(this.getApplicationContext(), ActivityMessageMediaView.class);
            intent.putExtra("filePath", filePath);
            intent.putExtra("mediaType", view.getContentDescription());
            startActivity(intent);
        }
    }

    private void sendTextMessage(final String[] recipientIds, final byte[] payload) {
        try {
            byte[] encodedPayload = Base64Utils.encode(payload);
            for (String recipientId : recipientIds) {
                final Map<String,String> headers = new HashMap<>();
                headers.put("messageId", String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf(payload.hashCode()));
                headers.put("messengerId", deviceId);
                headers.put("messengerPhone", devicePhone);
                headers.put("sessionId", sessionId);
                headers.put("content-type", MimeTypes.TEXT_PLAIN.getValue());
                //headers.put("ACK", "client");//client-individual
                taskText.stomp.sendMessage(this, String.format(SystemUtils.getResourceString(getResources(), R.string.textStreamChannel), recipientId),
                        headers,
                        encodedPayload);
            }
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        } finally {
            ActivityUtils.appendMessage((LinearLayout) findViewById(R.id.scrollPanel),
                    getLayoutInflater().inflate(R.layout.activity_message_text_right, null),
                    R.id.textViewRight,
                    MimeTypes.TEXT_PLAIN,
                    payload);
            editText.setText(StringUtils.EMPTY);
        }
    }

    private View sendImageMessage(final String[] recipientIds, final byte[] payload) {
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_message_image_right, null);
        try {
            byte[] encodedPayload = Base64Utils.encode(payload);
            for (String recipientId : recipientIds) {
                final Map<String, String> headers = new HashMap<>();
                final String messageId = String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf(payload.hashCode());
                headers.put("messageId", messageId);
                headers.put("messengerId", deviceId);
                headers.put("messengerPhone", devicePhone);
                headers.put("sessionId", sessionId);
                headers.put("content-type", MimeTypes.OCTET_STREAM.getValue());
                headers.put("media-type", SystemUtils.getResourceString(getResources(), R.string.label_media_type_image));
                //headers.put("receipt", headers.get("messageId"));
                progressTasks.put(messageId, ((ProgressBar) linearLayout.findViewById(R.id.progressBar)));
                taskImage.stomp.sendMessage(this, String.format(SystemUtils.getResourceString(getResources(), R.string.imageStreamChannel), recipientId),
                        headers,
                        encodedPayload);
//                ProgressTask progressTask = new ProgressTask();
//                progressTask.execute(messageTask, ((ProgressBar) linearLayout.findViewById(R.id.progressBar)));
            }
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        } finally {
            return ActivityUtils.appendMessage((LinearLayout) findViewById(R.id.scrollPanel),
                    linearLayout,
                    R.id.imageViewRight,
                    MimeTypes.OCTET_STREAM,
                    payload);
        }
    }

    private View sendFileMessage(final String[] recipientIds, final String mediaType, final String path) {
        File file = new File(path);
        try {
            byte[] payload = FileUtils.readBytes(file);
            if (!FileUtils.contains(getApplicationContext().getFilesDir().listFiles(), file.getName())) {
                MediaUtils.save(getApplicationContext(), file.getName(), payload);
            }
            byte[] encodedPayload = Base64Utils.encode(payload);
            for (String recipientId : recipientIds) {
                Map<String, String> headers = new HashMap<>();
                headers.put("messageId", String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf(payload.hashCode()));
                headers.put("messengerId", deviceId);
                headers.put("messengerPhone", devicePhone);
                headers.put("sessionId", sessionId);
                headers.put("content-type", MimeTypes.OCTET_STREAM.getValue());
                headers.put("media-type", mediaType);

                taskFile.stomp.sendMessage(this, String.format(SystemUtils.getResourceString(getResources(), R.string.fileStreamChannel), recipientId),
                        headers,
                        encodedPayload);
            }
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        } finally {
            View view = ActivityUtils.appendImage((LinearLayout) findViewById(R.id.scrollPanel),
                    getLayoutInflater().inflate(R.layout.activity_message_image_right, null),
                    R.id.imageViewRight,
                    ActivityUtils.getCoverImage(getResources(), mediaType, path));
            view.setContentDescription(mediaType);
            view.setId(MediaUtils.getHashFromVideoFilename(getResources(), file.getName()));
            return view;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        final String item = (String) parent.getItemAtPosition(position);
        final String phoneNumber = (String) parent.getContentDescription();
        if (SystemUtils.getResourceString(getResources(), R.string.label_BlockSender).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            new BlockDeviceTask(getResources()).execute(tm.getDeviceId(), contentDescription);
            contentDescription = StringUtils.EMPTY;
            resetView();
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_RequestContact).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            ActivityUtils.sendContactRequest(taskText, getResources(), sessionId, tm.getDeviceId(), tm.getLine1Number(), contentDescription, R.string.label_media_type_contact_request);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_EditContact).equalsIgnoreCase(item)) {
            // Creates a new Intent to edit a contact
            final Intent intent = ActivityUtils.startActivityEditContact(this, phoneNumber);
            startActivity(intent);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_Cancel).equalsIgnoreCase(item)) {
            resetView();
        } else {
            // Checking camera availability
            if (!MediaUtils.isDeviceSupportsCamera(getApplicationContext())) {
                Toast.makeText(getApplicationContext(),
                        SystemUtils.getResourceString(getResources(), R.string.message_NoCameraSupport),
                        Toast.LENGTH_LONG).show();
                // will close the app if the device does't have camera
                //finish();
            } else {
                if (SystemUtils.getResourceString(getResources(), R.string.label_TakePhoto).equalsIgnoreCase(item)) {
                    captureImage();
                } else if (SystemUtils.getResourceString(getResources(), R.string.label_RecordVideo).equalsIgnoreCase(item)) {
                    recordVideo();
                } else if (SystemUtils.getResourceString(getResources(), R.string.label_MediaLibrary).equalsIgnoreCase(item)) {
                    getMediaContent();
                }
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            final String text = v.getText().toString();
            if (sessionIds != null && !sessionIds.isEmpty()) {
                sendTextMessage(sessionIds.toArray(new String[sessionIds.size()]), text.getBytes());
            } else {
                sendTextMessage(deviceIds, text.getBytes());
            }
            handled = true;
        }
        return handled;
    }

    private void processMessage(final MimeTypes type, final String mediaType, final String messengerId, final String deviceId, final String devicePhone, final byte[] bytes) {
        if (MimeTypes.OCTET_STREAM.equals(type)) {
            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_message_image_left, null);
            linearLayout.setContentDescription(deviceId);
            linearLayout.setOnLongClickListener(this);
            linearLayout.setTag(devicePhone);

            if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_image).equals(mediaType)) {
                final View view = ActivityUtils.appendMessage((LinearLayout) findViewById(R.id.scrollPanel),
                        linearLayout,
                        R.id.imageViewLeft,
                        MimeTypes.OCTET_STREAM,
                        bytes);
                final Uri uri = MediaUtils.getOutputMediaFileUri(getResources(),
                        MediaUtils.MEDIA_TYPE_IMAGE,
                        SystemUtils.getResourceString(getResources(), R.string.image_directory_name));
                final String fileName = new File(uri.getPath()).getName();
                MediaUtils.save(getApplicationContext(), fileName, bytes);
                view.setId(MediaUtils.getHashFromImageFilename(getResources(), fileName));
                storedMedia.add(uri.getPath());
            } else if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_video).equals(mediaType)) {
                final Uri uri = MediaUtils.getOutputMediaFileUri(getResources(),
                        MediaUtils.MEDIA_TYPE_VIDEO,
                        SystemUtils.getResourceString(getResources(), R.string.image_directory_name));
                final File file = new File(uri.getPath());
                MediaUtils.save(getApplicationContext(), file.getName(), bytes);
                final View view = ActivityUtils.appendImage((LinearLayout) findViewById(R.id.scrollPanel),
                        linearLayout,
                        R.id.imageViewLeft,
                        ActivityUtils.getCoverImage(getResources(), mediaType, file.getPath()));
                view.setContentDescription(mediaType);
                view.setId(MediaUtils.getHashFromVideoFilename(getResources(), file.getName()));
                storedMedia.add(uri.getPath());
            }
            ActivityUtils.setImageViewBackgroundColor((ImageView) linearLayout.getChildAt(linearLayout.getChildCount() - 1), colorMap, type, messengerId);
        } else if (MimeTypes.TEXT_PLAIN.equals(type)) {
            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_message_text_left, null);
            linearLayout.setContentDescription(deviceId);
            linearLayout.setOnLongClickListener(this);
            linearLayout.setTag(devicePhone);
            ActivityUtils.appendMessage((LinearLayout) findViewById(R.id.scrollPanel),
                    linearLayout,
                    R.id.textViewLeft,
                    type,
                    bytes);
            ActivityUtils.setTextColor((TextView) linearLayout.getChildAt(linearLayout.getChildCount() - 1), colorMap, type, messengerId);

            if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_request).equals(mediaType)) {
                linearLayout.setOnClickListener(this);
            }
        }
    }

    @Override
    public void onProgressUpdate(final String messageId, final float from, final float to) {
        final ProgressBar progressBar = progressTasks.get(messageId);
        if (progressBar != null) {
            ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, from, to);
            anim.setDuration(1000);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (to == 100) {
                        progressBar.setVisibility(View.INVISIBLE);
                        progressTasks.remove(messageId);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            progressBar.startAnimation(anim);
        }
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) { EmojiconsFragment.input(editText, emojicon); }

    @Override
    public void onEmojiconBackspaceClicked(View view) {
        EmojiconsFragment.backspace(editText);
    }

    @Override
    public void onClick(View view) {
        if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_request).equals(mediaType)) {
            final String phoneNumber = view.getTag().toString();
            // Creates a new Intent to edit a contact
            final Intent intent = ActivityUtils.startActivityInsertContact(phoneNumber);
            startActivity(intent);
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            ActivityUtils.sendContactRequest(taskText, getResources(), sessionId, tm.getDeviceId(), tm.getLine1Number(), view.getContentDescription().toString(), R.string.label_media_type_contact_accept);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        contentDescription = view.getContentDescription().toString();
        SystemUtils.hideKeyboard(this.getApplicationContext(), editText, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
        findViewById(R.id.editorPanel).setVisibility(View.GONE);
        findViewById(R.id.emojiViewPanel).setVisibility(View.GONE);

        final String phoneNumber = view.getTag().toString();
        final boolean isContactExists = MapUtils.isContactExists(this, phoneNumber);
        final StableArrayAdapter adapterUserActions = new StableArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                SystemUtils.getResourcesString(getResources(), new int[]{R.string.label_BlockSender,
                                                                         (!isContactExists) ? R.string.label_RequestContact : R.string.label_EditContact,
                                                                         R.string.label_Cancel}));

        final ListView listViewUserActions = (ListView) findViewById(R.id.listViewUserActions);
        listViewUserActions.setAdapter(adapterUserActions);
        listViewUserActions.setOnItemClickListener(this);
        listViewUserActions.setContentDescription(phoneNumber);

        findViewById(R.id.listViewPanelUserActions).setVisibility(View.VISIBLE);
        findViewById(R.id.listViewPanelEditActions).setVisibility(View.GONE);
        ((ScrollView) findViewById(R.id.scrollView)).fullScroll(View.FOCUS_DOWN);
        return true;
    }

    private class StompWsTask extends AsyncTask<String, Object, Stomp> implements MessageHandlerTask {
        private Stomp stomp;
        private ScrollView scrollView;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            scrollView = (ScrollView) findViewById(R.id.scrollView);
        }

        @Override
        protected void onProgressUpdate(final Object... values) {
            super.onProgressUpdate(values);
            if (values != null && values.length > 0) {
                final MimeTypes type = MimeTypes.fromValue((String) values[0]);
                final byte[] bytes = (byte[]) values[5];
                processMessage(type, (String) values[1], (String) values[2], (String) values[3], (String) values[4], bytes);
            }
        }

        @Override
        protected Stomp doInBackground(final String... params) {
            final Map<String,String> headersSetup = new HashMap<>();
            final Stomp s = new Stomp(params[0], headersSetup, new ListenerWSNetwork() {
                @Override
                public void onState(int state) {
                    int s = state;
                }
            });
            try {
                s.connect();
                s.subscribe(new Subscription(params[1], new ListenerSubscription() {
                    @Override
                    public void onMessage(final Map<String, String> headers, final byte[] payload) {
                        final String mediaType = headers.get("media-type");
                        final String messengerId = headers.get("sessionId");
                        ActivityUtils.add(messengerId, sessionIds);
                        ActivityUtils.add(messengerId, colorMap, SystemUtils.getResourcesColor(getResources(), new int[]{R.integer.color_map_threshold_r, R.integer.color_map_threshold_g, R.integer.color_map_threshold_b}));
                        final byte[] body = Base64Utils.decode(payload);

                        if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_accept).equals(mediaType)) {
                            final String phoneNumber = new String(body);
                            final Intent intent = ActivityUtils.startActivityInsertContact(phoneNumber);
                            startActivity(intent);
                        } else {
                            publishProgress(headers.get("content-type"), headers.get("media-type"), messengerId, headers.get("messengerId"), headers.get("messengerPhone"), body);
                        }
//                        final Map<String, String> rheaders = new HashMap<String, String>();
//                        rheaders.put("message-id", headers.get("message-id"));
                        //rheaders.put("subscription", headers.get("subscription"));
//                        s.transmit("ACK", rheaders, null);
                    }

                    @Override
                    public void onMessageStream(final Map<String, String> headers, final byte[] payload) {

                    }
                }));
                return s;
            } catch (Exception e) {
                Log.e("MessageActivity", e.getMessage(), e);
            } finally {
                return s;
            }
        }

        @Override
        protected void onPostExecute(final Stomp s) {
            this.stomp = s;
        }

        @Override
        public Stomp getMessageHandler() {
            return stomp;
        }
    }

    /**
     * Capturing Camera Image will launch camera app and request image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = MediaUtils.getOutputMediaFileUri(getResources(),
                                                   MediaUtils.MEDIA_TYPE_IMAGE,
                                                   SystemUtils.getResourceString(getResources(), R.string.image_directory_name));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, MediaUtils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Recording video
     */
    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = MediaUtils.getOutputMediaFileUri(getResources(),
                                                   MediaUtils.MEDIA_TYPE_VIDEO,
                                                   SystemUtils.getResourceString(getResources(), R.string.image_directory_name));
        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the video capture Intent
        startActivityForResult(intent, MediaUtils.CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    /**
     * Capturing Camera Image will launch camera app and request image capture
     */
    private void getMediaContent() {
        final Intent intent = new Intent(Intent.ACTION_PICK); //, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(SystemUtils.getResourceString(getResources(), R.string.intent_type_media));
        startActivityForResult(intent, MediaUtils.SELECT_PICTURE_ACTIVITY_REQUEST_CODE);
    }
    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null if screen orientation changes
        outState.putParcelable("fileUri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("fileUri");
    }

    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == MediaUtils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                sendCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                               SystemUtils.getResourceString(getResources(), R.string.message_ImageCaptureCancelled), Toast.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                               SystemUtils.getResourceString(getResources(), R.string.message_ImageCaptureFailed), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == MediaUtils.CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                sendRecordedVideo();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                               SystemUtils.getResourceString(getResources(), R.string.message_VideoRecordingCancelled), Toast.LENGTH_SHORT).show();
            } else {
                // failed to record video
                Toast.makeText(getApplicationContext(),
                               SystemUtils.getResourceString(getResources(), R.string.message_VideoRecordingFailed), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == MediaUtils.SELECT_PICTURE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri selectedItem = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedItem, filePathColumn, null, null, null);
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    String fileName = new File(filePath).getName();
                    if (MediaUtils.isImage(getResources(), fileName)) {
                        View view = sendImage(filePath);
                        view.setId(MediaUtils.getHashFromImageFilename(getResources(), fileName));
                    } else if (MediaUtils.isVideo(getResources(), fileName)){
                        View view = sendVideo(filePath);
                        view.setId(MediaUtils.getHashFromVideoFilename(getResources(), fileName));
                    }
                }
                cursor.close();
            }
        }
    }

    private View sendImage(final String path) {
        View view = null;
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            final Bitmap subsampledBitmap = BitmapFactory.decodeFile(path, options);
            final byte[] subsampledBytes = MediaUtils.getImageBytes(subsampledBitmap);

            if (sessionIds != null && !sessionIds.isEmpty()) {
                view = sendImageMessage(sessionIds.toArray(new String[sessionIds.size()]), subsampledBytes);
            } else {
                view = sendImageMessage(deviceIds, subsampledBytes);
            }
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        } finally {
            return view;
        }
    }

    private View sendVideo(final String path) {
        View view = null;
        try {
            if (sessionIds != null && !sessionIds.isEmpty()) {
                view = sendFileMessage(sessionIds.toArray(new String[sessionIds.size()]), SystemUtils.getResourceString(getResources(), R.string.label_media_type_video), path);
            } else {
                view = sendFileMessage(deviceIds, SystemUtils.getResourceString(getResources(), R.string.label_media_type_video), path);
            }
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        } finally {
            return view;
        }
    }

    private void sendCapturedImage() {
        final View view = sendImage(fileUri.getPath());
        final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath());
        final byte[] bytes = MediaUtils.getImageBytes(bitmap);
        File file = new File(fileUri.getPath());
        MediaUtils.save(getApplicationContext(), file.getName(), bytes);
        view.setId(MediaUtils.getHashFromImageFilename(getResources(), file.getName()));
        addToGallery(fileUri);
    }

    private void addToGallery(Uri uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        storedMedia.add(FileUtils.getPath(getApplicationContext().getFilesDir().listFiles(), new File(uri.getPath()).getName()));
        this.sendBroadcast(mediaScanIntent);
    }

    private void sendRecordedVideo() {
        try {
            sendVideo(fileUri.getPath());
            addToGallery(fileUri);
        } catch (Exception e) {
            Log.e("MessageActivity", e.getMessage(), e);
        }
    }
}
