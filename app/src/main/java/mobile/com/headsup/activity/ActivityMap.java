package mobile.com.headsup.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Polygon;
import com.androidmapsextensions.PolygonOptions;
import com.androidmapsextensions.Polyline;
import com.androidmapsextensions.PolylineOptions;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import mobile.com.headsup.adapter.StableArrayAdapter;
import mobile.com.headsup.application.ApplicationLifecycleManager;
import mobile.com.headsup.async.BlockDeviceTask;
import mobile.com.headsup.async.MessageHandlerTask;
import mobile.com.headsup.async.UpdateDeviceTask;
import mobile.com.headsup.device.CacheKey;
import mobile.com.headsup.device.Cacheable;
import mobile.com.headsup.map.CustomMapFragment;
import mobile.com.headsup.device.Device;
import mobile.com.headsup.map.MapWrapperLayout;
import mobile.com.headsup.R;
import mobile.com.headsup.cluster.Cluster;
import mobile.com.headsup.cluster.ClusterManager;
import mobile.com.headsup.enums.DeviceStatus;
import mobile.com.headsup.enums.MimeTypes;
import mobile.com.headsup.stompws.ListenerSubscription;
import mobile.com.headsup.stompws.ListenerWSNetwork;
import mobile.com.headsup.stompws.Stomp;
import mobile.com.headsup.stompws.Subscription;
import mobile.com.headsup.utils.ActivityUtils;
import mobile.com.headsup.utils.ClusterUtils;
import mobile.com.headsup.utils.DeviceUtils;
import mobile.com.headsup.utils.JsonUtils;
import mobile.com.headsup.utils.MapUtils;
import mobile.com.headsup.utils.StringUtils;
import mobile.com.headsup.utils.SystemUtils;

public class ActivityMap extends FragmentActivity implements GoogleMap.OnMapLoadedCallback, GoogleMap.OnCameraChangeListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, MapWrapperLayout.OnDragListener, ClusterManager.Listener, AdapterView.OnItemClickListener, ApplicationLifecycleManager.Listener {

    private GoogleMap map; // Might be null if Google Play services APK is not available.
    private String recipientId;
    private SortedMap<String, Marker> markers;
    private ClusterManager<Cluster> clusterManager;
    private StompWsTask taskPositionUpdate;
    private StompWsTask taskText;
    private boolean isMapFrozen = false;
    private boolean isMapLoaded = false;
    private Polygon polygon;
    private Polyline polyline;
    private PolygonOptions polygonOptions;
    private PolylineOptions polylineOptions;
    private List<Polygon> polygons;
    private List<Polyline> polylines;
    private List<LatLng> selectedPositions;
    private Projection projection;
    private boolean isInfoWindowShown = true;
    //private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        setUpMapIfNeeded();
        /*
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
                */

        polygons = new ArrayList<>();
        polylines = new ArrayList<>();
        selectedPositions = new ArrayList<>();

        ApplicationLifecycleManager.get(getApplication()).registerListener(this);

        final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        taskPositionUpdate = new StompWsTask();
        taskPositionUpdate.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.positionStreamChannel), tm.getDeviceId()));

        taskText = new StompWsTask();
        taskText.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.textStreamChannel), tm.getDeviceId()));

        new StompWsTask().execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.imageStreamChannel), tm.getDeviceId()));

        new StompWsTask().execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.fileStreamChannel), tm.getDeviceId()));
    }

    public void setContentView(View view) {
        if (view.getId() == R.id.imageButtonOutline) {
            isMapFrozen = !isMapFrozen;
            UiSettings uiSettings = map.getUiSettings();
            uiSettings.setAllGesturesEnabled(!isMapFrozen);
            if (isMapFrozen) {
                projection = map.getProjection();
                polygonOptions = new PolygonOptions();
                polygonOptions.strokeColor(Color.RED);
                polygonOptions.geodesic(true);
                polylineOptions = new PolylineOptions();
                polylineOptions.color(Color.RED);
                polylineOptions.geodesic(true);
            } else {
                MapUtils.clearPolylines(polylines);
                MapUtils.clearPolylineOptions(polylineOptions);
                MapUtils.clearPolygons(polygons);
                MapUtils.clearPolygonOptions(polygonOptions);

                if (selectedPositions != null && !selectedPositions.isEmpty()) {
                    final List<Marker> recipients = MapUtils.getMarkers(new ArrayList<>(markers.values()), selectedPositions);
                    if (!recipients.isEmpty()) {
                        message(MapUtils.getDeviceIds(recipients).toArray(new String[recipients.size()]));
                    }
                    selectedPositions.clear();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBecameForeground() { sendUpdate(DeviceStatus.ONLINE); }

    @Override
    public void onBecameBackground() { sendUpdate(DeviceStatus.STANDBY); }

    @Override
    public void onApplicationDestroyed() {
        sendUpdate(DeviceStatus.OFFLINE);
        Log.i("ActivityMap", "DeviceStatus.OFFLINE sent");
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {
        //Log.d("ActivityMap", String.format("%s", motionEvent));
        UiSettings uiSettings = map.getUiSettings();
        if (isMapFrozen && (motionEvent.getAction() == MotionEvent.ACTION_DOWN || motionEvent.getAction() == MotionEvent.ACTION_MOVE || motionEvent.getAction() == MotionEvent.ACTION_UP)) {
            final LatLng latLng = projection.fromScreenLocation(new Point((int) motionEvent.getX(), (int) motionEvent.getY()));
            polygonOptions.add(latLng);
            polylineOptions.add(latLng);

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                polygon = map.addPolygon(polygonOptions);
                polygons.add(polygon);

                final List<LatLng> boundedPositions = MapUtils.getPositions(MapUtils.getWithin(polygonOptions.getPoints(), markers));
                selectedPositions.removeAll(boundedPositions);
                selectedPositions.addAll(boundedPositions);

                final List<Marker> recipients = MapUtils.getMarkers(new ArrayList<>(markers.values()), selectedPositions);

                MapUtils.updateMarkersIconColor(recipients, BitmapDescriptorFactory.HUE_YELLOW);
                MapUtils.clearPolylines(polylines);
                MapUtils.clearPolylineOptions(polylineOptions);
                MapUtils.clearPolygonOptions(polygonOptions);
            } else {
                MapUtils.clearPolygons(polygons);
                polyline = map.addPolyline(polylineOptions);
                polylines.add(polyline);
            }
        }
    }

    private class StompWsTask extends AsyncTask<String, Device, Stomp> implements MessageHandlerTask {
        private Stomp stomp;

        @Override
        protected void onProgressUpdate(final Device... devices) {
            super.onProgressUpdate(devices);
            if (devices != null && devices.length > 0) {
                final Device device = devices[0];
                if (device != null) {
                    MapUtils.removeMarker(map, markers, device);
                    MapUtils.addMarker(ActivityMap.this, map, markers, device);
                }
            }
        }

        @Override
        protected Stomp doInBackground(final String... params) {
            final Map<String,String> headersSetup = new HashMap<>();
            final Stomp s = new Stomp(params[0], headersSetup, new ListenerWSNetwork() {
                @Override
                public void onState(int state) {
                    int s = state;
                }
            });
            try {
                s.connect();
                s.subscribe(new Subscription(params[1], new ListenerSubscription() {
                    @Override
                    public void onMessage(final Map<String, String> headers, final byte[] payload) {
                        final String mediaType = headers.get("media-type");

                        if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_device).equals(mediaType)) {
                            final String json = new String(Base64Utils.decode(payload));
                            publishProgress((Device) JsonUtils.readValue(json, Device.class));
                        } else if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_accept).equals(mediaType)) {
                            final String phoneNumber = new String(Base64Utils.decode(payload));
                            final Intent intent = ActivityUtils.startActivityInsertContact(phoneNumber);
                            startActivity(intent);
                        } else {
                            final String messengerId = headers.get("messengerId");
                            final String messengerPhone = headers.get("messengerPhone");

                            recipientId = headers.get("sessionId");
                            Intent intent = new Intent(getApplicationContext(), ActivityNotification.class);
                            intent.putExtra("messengerId", messengerId);
                            intent.putExtra("messengerPhone", messengerPhone);
                            intent.putExtra("recipientId", recipientId);
                            intent.putExtra("contentType", headers.get("content-type"));
                            intent.putExtra("mediaType", mediaType);
                            intent.putExtra("message", Base64Utils.decode(payload));
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onMessageStream(final Map<String, String> headers, final byte[] payload) {

                    }
                }));
                return s;
            } catch (Exception e) {
                Log.e("MapsActivity", e.getMessage(), e);
            } finally {
                return s;
            }
        }

        @Override
        protected void onPostExecute(final Stomp s) {
            this.stomp = s;
        }

        @Override
        public Stomp getMessageHandler() {
            return stomp;
        }
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #map} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
            // Google map init block
            final CustomMapFragment customMapFragment = ((CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
            customMapFragment.setOnDragListener(this);

            map = customMapFragment.getExtendedMap();

            // Check if we were successful in obtaining the map.
            if (map != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #map} is not null.
     */
    private void setUpMap() {
        if (map != null) {
            isMapLoaded = false;
            map.setMyLocationEnabled(true); //Makes the users current location visible by displaying a blue dot.
            map.setOnMapLoadedCallback(this);
            map.setOnMapClickListener(this);
            map.setOnInfoWindowClickListener(this);
            map.setOnCameraChangeListener(this);

            clusterManager = new ClusterManager<>(this, map);
            clusterManager.registerListener(this);

            final LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);//use of location services by firstly defining location manager.
            final String provider = lm.getBestProvider(new Criteria(), true);
            final Location location = lm.getLastKnownLocation(provider);

            if (location != null) {
                relocate(new LatLng(location.getLatitude(), location.getLongitude()), 14.6f);
            }
        }
    }

    public void relocate(final LatLng latlng, final float zoom) {
        map.moveCamera(CameraUpdateFactory.newLatLng(latlng)); //Moves the camera to users current longitude and latitude
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, zoom)); //Animates camera and zooms to preferred state on the user's current location.
    }

    @Override
    public void onMapLoaded() {
        isMapLoaded = true;
        sendUpdate(DeviceStatus.ONLINE);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (isMapFrozen) {
            final List<LatLng> positions = new ArrayList<>();
            positions.add(marker.getPosition());
            if (selectedPositions.contains(marker.getPosition())) {
                selectedPositions.removeAll(positions);
                MapUtils.updateMarkerIconColor(marker, BitmapDescriptorFactory.HUE_GREEN);
                clear();
            } else {
                selectedPositions.add(marker.getPosition());
                MapUtils.updateMarkerIconColor(marker, BitmapDescriptorFactory.HUE_YELLOW);
                clear();
            }
        } else {
            final Cacheable device = marker.getData();
            final String deviceId = device.getId();
            if (deviceId != null) {
                message(new String[]{deviceId});
            }
        }
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        final Cacheable device = marker.getData();
        final boolean isContactExists = MapUtils.isContactExists(this, device.getPhoneNumber());
        final StableArrayAdapter adapterUserActions = new StableArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                SystemUtils.getResourcesString(getResources(), new int[]{R.string.label_BlockSender,
                                                                         (!isContactExists) ? R.string.label_RequestContact : R.string.label_EditContact,
                                                                         R.string.label_Cancel}));

        final ListView listViewUserActions = (ListView) findViewById(R.id.listViewUserActions);
        listViewUserActions.setAdapter(adapterUserActions);
        listViewUserActions.setOnItemClickListener(this);
        listViewUserActions.setContentDescription(device.getPhoneNumber());
        final RelativeLayout layout = (RelativeLayout) findViewById(R.id.listViewPanelUserActions);
        layout.setContentDescription(device.getId());
        layout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onClusterClick(Marker marker) {
        relocate(marker.getPosition(), map.getCameraPosition().zoom + 2.5f);
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (markers != null && !markers.isEmpty()) {
            final Collection<Marker> values = markers.values();
            for (Marker marker : values) {
                if (!StringUtils.isEmpty(marker.getTitle())) {
                    if (marker.isInfoWindowShown() || isInfoWindowShown) {
                        marker.hideInfoWindow();
                        isInfoWindowShown = false;
                    } else {
                        marker.showInfoWindow();
                        isInfoWindowShown = true;
                    }
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final String item = (String) parent.getItemAtPosition(position);
        final String deviceId = (String) ((RelativeLayout) parent.getParent()).getContentDescription();
        final String phoneNumber = (String) parent.getContentDescription();

        if (SystemUtils.getResourceString(getResources(), R.string.label_BlockSender).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            new BlockDeviceTask(getResources()).execute(tm.getDeviceId(), deviceId);
            findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_RequestContact).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            ActivityUtils.sendContactRequest(taskText, getResources(), tm.getDeviceId(), tm.getDeviceId(), tm.getLine1Number(), deviceId, R.string.label_media_type_contact_request);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_EditContact).equalsIgnoreCase(item)) {
            // Creates a new Intent to edit a contact
            final Intent intent = ActivityUtils.startActivityEditContact(this, phoneNumber);
            startActivity(intent);
        } else {
            findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        //Log.d("MapsActivity", "camera changed: " + cameraPosition.toString());
        if (isMapLoaded) {
            map.clear();
            sendUpdate(DeviceStatus.ONLINE);
        }
    }

    private void clear() {
        if (!polygonOptions.getPoints().isEmpty()) {
            final List<LatLng> boundedPositions = MapUtils.getPositions(MapUtils.getWithin(polygonOptions.getPoints(), markers));
            if (boundedPositions != null && boundedPositions.isEmpty()) {
                MapUtils.clearPolygons(polygons);
                MapUtils.clearPolylines(polylines);
                MapUtils.clearPolylineOptions(polylineOptions);
                MapUtils.clearPolygonOptions(polygonOptions);
            }
        }
    }

    private void message(final String[] deviceIds) {
        final Intent intent = new Intent(this.getApplicationContext(), ActivityMessage.class);
        intent.putExtra("deviceIds", deviceIds);

        final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        final String deviceId = tm.getDeviceId();
        intent.putExtra("sessionId", deviceId + String.valueOf(Calendar.getInstance().getTimeInMillis()));

        startActivity(intent);
    }

    private void sendUpdate(final DeviceStatus deviceStatus) {
        findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);

        final Location location = map.getMyLocation();
        if (location != null) {
            final VisibleRegion vr = map.getProjection().getVisibleRegion();

            clusterManager.clear();

            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

            new UpdateDeviceTask(getResources()).execute(
                    tm.getDeviceId(),
                    tm.getLine1Number(),
                    Double.valueOf(location.getLatitude()).toString(),
                    Double.valueOf(location.getLongitude()).toString(),
                    Double.valueOf(vr.latLngBounds.southwest.latitude).toString(),
                    Double.valueOf(vr.latLngBounds.southwest.longitude).toString(),
                    Double.valueOf(vr.latLngBounds.northeast.latitude).toString(),
                    Double.valueOf(vr.latLngBounds.northeast.longitude).toString(),
                    deviceStatus.getKey());

            final double area = MapUtils.area(vr.latLngBounds.southwest.latitude, vr.latLngBounds.southwest.longitude, vr.latLngBounds.northeast.latitude, vr.latLngBounds.northeast.longitude, MapUtils.M);
            //Log.d("MapsActivity", "area: " + area);

            final int zoom = MapUtils.zoom(area, MapUtils.M);
            Log.d("MapsActivity", "final zoom: " + zoom);

            new FindClustersTask(deviceStatus).execute(
                    Double.valueOf(vr.latLngBounds.southwest.latitude).toString(),
                    Double.valueOf(vr.latLngBounds.southwest.longitude).toString(),
                    Double.valueOf(vr.latLngBounds.northeast.latitude).toString(),
                    Double.valueOf(vr.latLngBounds.northeast.longitude).toString(),
                    Integer.valueOf(zoom).toString());
        }
    }

    private class FindClustersTask extends AsyncTask<String, Void, List<Cluster>> {

        private DeviceStatus deviceStatus;

        public FindClustersTask(final DeviceStatus deviceStatus) {
            this.deviceStatus = deviceStatus;
        }

        @Override
        protected List<Cluster> doInBackground(final String... params) {
            Cluster[] clusters = null;
            try {
                final String url = getResources().getText(R.string.webserviceHostname).toString() + getResources().getText(R.string.webserviceEndpointFindClustersWithCounts).toString();
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                clusters = restTemplate.getForObject(url, Cluster[].class, (Object[]) params);

                return Arrays.asList(clusters);
            } catch (Exception e) {
                Log.e("MapsActivity", e.getMessage(), e);
                //Log.d("MapsActivity", clusters.toString());
            }
            return null;
        }

        protected void onPostExecute(final List<Cluster> clusters) {
            markers = Collections.synchronizedSortedMap(new TreeMap<String, Marker>());

            if (clusters != null) {
                final VisibleRegion vr = map.getProjection().getVisibleRegion();
                final double area = MapUtils.area(vr.latLngBounds.southwest.latitude, vr.latLngBounds.southwest.longitude, vr.latLngBounds.northeast.latitude, vr.latLngBounds.northeast.longitude, MapUtils.M);
                final int zoom = MapUtils.zoom(area, MapUtils.M);
                /* for debugging purposes .. uncomment to see the relation of clusters to data points
                final Location location = map.getMyLocation();
                if (location != null) {
                    final LatLngBounds bounds = vr.latLngBounds;
                    final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                    new FindTask().execute(
                            tm.getDeviceId(),
                            Double.valueOf(vr.latLngBounds.southwest.latitude).toString(),
                            Double.valueOf(vr.latLngBounds.southwest.longitude).toString(),
                            Double.valueOf(vr.latLngBounds.northeast.latitude).toString(),
                            Double.valueOf(vr.latLngBounds.northeast.longitude).toString());
                }
                clusterManager.addAll(clusters);
                */
                if (ClusterUtils.getTotalSize(clusters) <= Integer.valueOf(SystemUtils.getResourceString(getResources(), R.string.map_max_marker_threshold)) || zoom >= Integer.valueOf(SystemUtils.getResourceString(getResources(), R.string.map_min_zoom_threshold))) {
                    clusterManager.setOnMarkerClickListener(ActivityMap.this);
                    final Location location = map.getMyLocation();
                    if (location != null) {
                        final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                        new FindTask().execute(
                                tm.getDeviceId(),
                                Double.valueOf(vr.latLngBounds.southwest.latitude).toString(),
                                Double.valueOf(vr.latLngBounds.southwest.longitude).toString(),
                                Double.valueOf(vr.latLngBounds.northeast.latitude).toString(),
                                Double.valueOf(vr.latLngBounds.northeast.longitude).toString());
                    }
                } else {
                    clusterManager.setOnMarkerClickListener(clusterManager);
                    clusterManager.addAll(clusters);
                }
            }
        }
    }

    private class FindTask extends AsyncTask<String, Void, List<Cacheable>> {
        @Override
        protected List<Cacheable> doInBackground(final String... params) {
            try {
                final String url = getResources().getText(R.string.webserviceHostname).toString() + getResources().getText(R.string.webserviceEndpointFind).toString();
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                final String imei = params[0];
                final Cacheable[] devices = restTemplate.getForObject(url, CacheKey[].class, (Object[]) Arrays.copyOfRange(params, 1, params.length));
                final List<Cacheable> deviceList = new ArrayList<>(Arrays.asList(devices));
                final Cacheable myDevice = DeviceUtils.remove(deviceList, imei);

                notifyPositionUpdate(myDevice, deviceList);
                deviceList.add(0, myDevice);

                return deviceList;
            } catch (Exception e) {
                Log.e("MapsActivity", e.getMessage(), e);
            }
            return null;
        }

        protected void onPostExecute(final List<Cacheable> devices) {
            if (devices != null) {
                final Cacheable myDevice = devices.remove(0);

                for (Cacheable device : devices) {
                    if (!device.getBlocked().contains(myDevice.getId())) {
                        MapUtils.addMarker(ActivityMap.this, map, markers, device);
                    } else if (myDevice.getBlocked().contains(device.getId())) {
                        final Marker marker = MapUtils.addMarker(ActivityMap.this, map, markers, device);
                        // TODO: make this marker not clickable
                        // TODO: this is a blocked device .. perhaps the marker shape should be different .. not just color
                        MapUtils.updateMarkerIconColor(marker, BitmapDescriptorFactory.HUE_RED);
                    }
                }
            }
        }
    }

    private void notifyPositionUpdate(final Cacheable source, final List<Cacheable> devices) {
        try {
            if (!devices.isEmpty()) {
                byte[] payload = JsonUtils.writeValue(source).getBytes();
                byte[] encodedPayload = Base64Utils.encode(payload);
                for (Cacheable device : devices) {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("messageId", String.valueOf(Calendar.getInstance().getTimeInMillis()) + String.valueOf(payload.hashCode()));
                    headers.put("messengerId", source.getId());
                    headers.put("content-type", MimeTypes.TEXT_PLAIN.getValue());
                    headers.put("media-type", SystemUtils.getResourceString(getResources(), R.string.label_media_type_device));
                    //headers.put("ACK", "client");//client-individual
                    taskPositionUpdate.stomp.sendMessage(null, String.format(SystemUtils.getResourceString(getResources(), R.string.positionStreamChannel), device.getId()),
                            headers,
                            encodedPayload);
                }
            }
        } catch (Exception e) {
            Log.e("MapActivity", e.getMessage(), e);
        }
    }
}
