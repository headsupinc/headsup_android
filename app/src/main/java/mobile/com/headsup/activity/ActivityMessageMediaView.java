package mobile.com.headsup.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.VideoView;

import java.io.File;

import mobile.com.headsup.R;
import mobile.com.headsup.utils.MediaUtils;
import mobile.com.headsup.utils.SystemUtils;

/**
 * Created by Vadim on 9/7/2015.
 */
public class ActivityMessageMediaView extends Activity {

    private String filePath;
    private String mediaType;
    private byte[] message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        filePath = extras.getString("filePath");
        mediaType = extras.getString("mediaType");

        if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_image).equalsIgnoreCase(mediaType)) {
            setContentView(R.layout.activity_message_image_view);
            ImageView imageView = (ImageView) findViewById(R.id.activityMessageImageView);
            Bitmap bitmap = MediaUtils.loadBitmap(getApplicationContext(), new File(filePath).getName());
            imageView.setImageBitmap(bitmap);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_video).equalsIgnoreCase(mediaType)) {
            setContentView(R.layout.activity_message_video_view);
            VideoView videoView = (VideoView) findViewById(R.id.activityMessageVideoView);
            videoView.setVideoPath(filePath);
            videoView.start();
        }
    }
}
