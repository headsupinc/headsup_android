package mobile.com.headsup.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.com.headsup.R;
import mobile.com.headsup.adapter.StableArrayAdapter;
import mobile.com.headsup.async.BlockDeviceTask;
import mobile.com.headsup.async.MessageHandlerTask;
import mobile.com.headsup.device.Device;
import mobile.com.headsup.enums.MimeTypes;
import mobile.com.headsup.stompws.ListenerWSNetwork;
import mobile.com.headsup.stompws.Stomp;
import mobile.com.headsup.utils.ActivityUtils;
import mobile.com.headsup.utils.ByteUtils;
import mobile.com.headsup.utils.FileUtils;
import mobile.com.headsup.utils.MediaUtils;
import mobile.com.headsup.utils.StringUtils;
import mobile.com.headsup.utils.SystemUtils;

/**
 * Created by Vadim on 9/7/2015.
 */
public class ActivityNotification extends Activity implements AdapterView.OnItemClickListener, View.OnTouchListener, View.OnLongClickListener {

    private String messengerId;
    private String messengerPhone;
    private String recipientId;
    private String contentType;
    private String mediaType;
    private byte[] message;

    private StompWsTask taskText;
    private List<String> storedMedia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        storedMedia = new ArrayList<>();
        messengerId = extras.getString("messengerId");
        messengerPhone = extras.getString("messengerPhone");
        recipientId = extras.getString("recipientId");
        contentType = extras.getString("contentType");
        mediaType = extras.getString("mediaType");
        message = extras.getByteArray("message");

        if (!ByteUtils.isEmpty(message) && !StringUtils.isEmpty(contentType) && !StringUtils.isEmpty(recipientId)) {
            final MimeTypes type = MimeTypes.fromValue(contentType);
            processMessage(type, mediaType, message);
        }

        LinearLayout linearlayout = (LinearLayout)findViewById(R.id.dialogNotification);
        linearlayout.setOnTouchListener(this);

        final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        taskText = new StompWsTask();
        taskText.execute(
                SystemUtils.getResourceString(getResources(), R.string.websocketEndpointUrl),
                String.format(SystemUtils.getResourceString(getResources(), R.string.textStreamChannel), tm.getDeviceId()));
    }

    protected void onDestroy() {
        super.onDestroy();
        FileUtils.deleteFiles(getApplicationContext().getFilesDir(), storedMedia);
    }

    private void processMessage(final MimeTypes type, final String mediaType, final byte[] bytes) {
        if (MimeTypes.OCTET_STREAM.equals(type)) {
            setContentView(R.layout.activity_notification_image);
            ImageView imageView = (ImageView) findViewById(R.id.imageViewNotification);
            Bitmap bitmap = null;
            if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_image).equals(mediaType)) {
                bitmap = MediaUtils.getImage(bytes);
            } else if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_video).equals(mediaType)) {
                Uri uri = MediaUtils.getOutputMediaFileUri(getResources(),
                        MediaUtils.MEDIA_TYPE_VIDEO,
                        SystemUtils.getResourceString(getResources(), R.string.image_directory_name));
                File file = new File(uri.getPath());
                MediaUtils.save(getApplicationContext(), file.getName(), bytes);
                bitmap = ActivityUtils.getCoverImage(getResources(), mediaType, file.getPath());
                storedMedia.add(uri.getPath());
            }
            Bitmap cropped = MediaUtils.cropToSquare(bitmap);
            ActivityUtils.appendMessage(imageView, type, MediaUtils.getImageBytes(cropped));
        } else if (MimeTypes.TEXT_PLAIN.equals(type)) {
            if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_request).equals(mediaType)) {
                setContentView(R.layout.activity_notification_contact);
                TextView textView = (TextView) findViewById(R.id.textViewNotification);
                final String phoneNumber = new String(bytes);
                textView.setText(phoneNumber);
            } else {
                setContentView(R.layout.activity_notification_text);
                TextView textView = (TextView) findViewById(R.id.textViewNotification);
                ActivityUtils.appendMessage(textView, type, message);
            }
        }

        final StableArrayAdapter adapterUserActions = new StableArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                SystemUtils.getResourcesString(getResources(), new int[]{R.string.label_BlockSender, R.string.label_Cancel}));

        final ListView listViewUserActions = (ListView) findViewById(R.id.listViewUserActions);
        listViewUserActions.setAdapter(adapterUserActions);
        listViewUserActions.setOnItemClickListener(this);
    }

    @Override
    public boolean onTouch(View var1, MotionEvent var2) {
        if (SystemUtils.getResourceString(getResources(), R.string.label_media_type_contact_request).equals(mediaType)) {
            final String phoneNumber = new String(message);
            // Creates a new Intent to edit a contact
            final Intent intent = ActivityUtils.startActivityInsertContact(phoneNumber);
            startActivity(intent);
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            ActivityUtils.sendContactRequest(taskText, getResources(), tm.getDeviceId(), tm.getDeviceId(), tm.getLine1Number(), messengerId, R.string.label_media_type_contact_accept);
            return false;
        } else {
            final Intent intent = new Intent(this.getApplicationContext(), ActivityMessage.class);
            intent.putExtra("recipientId", recipientId);
            intent.putExtra("messengerId", messengerId);
            intent.putExtra("messengerPhone", messengerPhone);
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            final String deviceId = tm.getDeviceId();
            intent.putExtra("sessionId", deviceId + String.valueOf(Calendar.getInstance().getTimeInMillis()));
            intent.putExtra("contentType", contentType);
            intent.putExtra("mediaType", mediaType);
            intent.putExtra("message", message);
            startActivity(intent);
            return true;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        findViewById(R.id.listViewPanelUserActions).setVisibility(View.VISIBLE);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        final String item = (String) parent.getItemAtPosition(position);

        if (SystemUtils.getResourceString(getResources(), R.string.label_BlockSender).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            new BlockDeviceTask(getResources()).execute(tm.getDeviceId(), messengerId);
            findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_RequestContact).equalsIgnoreCase(item)) {
            final TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            ActivityUtils.sendContactRequest(taskText, getResources(), tm.getDeviceId(), tm.getDeviceId(), tm.getLine1Number(), messengerId, R.string.label_media_type_contact_request);
            //startActivity(intent);
        } else if (SystemUtils.getResourceString(getResources(), R.string.label_EditContact).equalsIgnoreCase(item)) {
            // Creates a new Intent to edit a contact
            final Intent intent = ActivityUtils.startActivityEditContact(this, messengerPhone);
            startActivity(intent);
        } else {
            findViewById(R.id.listViewPanelUserActions).setVisibility(View.GONE);
        }
    }

    private class StompWsTask extends AsyncTask<String, Device, Stomp> implements MessageHandlerTask {
        private Stomp stomp;

        @Override
        protected Stomp doInBackground(final String... params) {
            final Map<String,String> headersSetup = new HashMap<>();
            final Stomp s = new Stomp(params[0], headersSetup, new ListenerWSNetwork() {
                @Override
                public void onState(int state) {
                    int s = state;
                }
            });
            try {
                s.connect();
                return s;
            } catch (Exception e) {
                Log.e("NotificationsActivity", e.getMessage(), e);
            } finally {
                return s;
            }
        }

        @Override
        protected void onPostExecute(final Stomp s) {
            this.stomp = s;
        }

        @Override
        public Stomp getMessageHandler() {
            return stomp;
        }
    }
}
