package mobile.com.headsup.cluster;

import android.app.Application;
import android.content.Context;
import android.util.Log;

//import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.Marker;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.GoogleMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mobile.com.headsup.utils.MapUtils;

/**
 * Created by Vadim on 11/12/2015.
 */
public class ClusterManager<T extends ClusterItem> implements GoogleMap.OnMarkerClickListener {

    private Context context;
    private GoogleMap map;
    private List<ClusterItem> clusters;
    private final List<Listener> listeners = new ArrayList<Listener>();

    public interface Listener {
        public boolean onClusterClick(Marker marker);
    }

    public ClusterManager() {
        clusters = Collections.synchronizedList(new ArrayList<ClusterItem>());
    }

    public ClusterManager(final Context context, final GoogleMap map) {
        this();
        this.context = context;
        this.map = map;
        //this.map.setOnMarkerClickListener(this);
    }

    public void setOnMarkerClickListener(final GoogleMap.OnMarkerClickListener listener) {
        this.map.setOnMarkerClickListener(listener);
    }

    public void registerListener(Listener listener) { listeners.add(listener); }
    public void unregisterListener(Listener listener) { listeners.remove(listener); }

    public void addAll(final List<Cluster> list) {
        if (!list.isEmpty()) {
            clear();
            clusters.addAll(list);
            cluster();
        }
    }
    public void addCluster(final Cluster cluster) {
        clusters.remove(cluster);
        clusters.add(cluster);
        cluster(cluster);
    }

    public void cluster() {
        for (ClusterItem item : clusters) {
            cluster(item);
        }
    }

    public void clear() {
        clusters.clear();
        map.clear();
    }

    private void cluster(final ClusterItem item) {
        MapUtils.addClusterItem(context, map, item);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return notifyOnClusterClick(marker);
    }

    private boolean notifyOnClusterClick(Marker marker) {
        boolean action = false;
        for (Listener listener : listeners) {
            try {
                action = listener.onClusterClick(marker);
            } catch (Exception e) {
                Log.e("ClusterManager", e.getMessage(), e);
            }
        }
        return action;
    }
}
