package mobile.com.headsup.cluster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.ui.IconGenerator;

import mobile.com.headsup.R;
import mobile.com.headsup.utils.ClusterUtils;

/**
 * Created by Vadim on 11/12/2015.
 */
public class Cluster implements ClusterItem {

    private String id;
    private Double latitude;
    private Double longitude;
    private int size;

    /*
    public Cluster(final float latitude, final float longitude, final int size) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.size = size;
    }
    */

    /*
    public void setPosition(final LatLng latLng) {
        this.latLng = latLng;
    }
    */
    public String getId() {
        return id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int getSize() {
        return size;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    @Override
    public Bitmap getIcon(final Context context) {
        final IconGenerator iconGen = new IconGenerator(context);

        // Define the size you want from dimensions file
        int shapeSize = ClusterUtils.getClusterSize(context.getResources(), size);
        int shapeColor = ClusterUtils.getClusterColor(context.getResources(), size);

        final GradientDrawable shapeDrawable = (GradientDrawable) ResourcesCompat.getDrawable(context.getResources(), R.drawable.cluster, null);
        shapeDrawable.setColor(shapeColor);
        iconGen.setBackground(shapeDrawable);

        final TextView textView = new TextView(context);
        textView.setLayoutParams(new ViewGroup.LayoutParams(shapeSize, shapeSize));
        textView.setText(ClusterUtils.getClusterText(context.getResources(), size));
        textView.setGravity(Gravity.CENTER);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(ClusterUtils.getClusterTextSize(context.getResources(), size));
        textView.setTextColor(context.getResources().getColor(R.color.color_map_cluster_text));
        iconGen.setContentView(textView);

        // Create the bitmap
        return iconGen.makeIcon();
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof Cluster)) {
            return false;
        }
        final Cluster other = (Cluster) object;
        return getPosition().equals(other.getPosition());
    }
}
