package mobile.com.headsup.cluster;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Vadim on 11/12/2015.
 */
public interface ClusterItem {
    public LatLng getPosition();
    public Bitmap getIcon(final Context context);
}

