package mobile.com.headsup.application;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vadim on 10/18/2015.
 */
// based on https://gist.github.com/xadh00m/1584a58ddb3d724cdd24
public class ApplicationLifecycleManager implements Application.ActivityLifecycleCallbacks {

    public static final long LIFECYCLE_DELAY = 500;

    private static ApplicationLifecycleManager instance;

    public interface Listener {
        public void onBecameForeground();
        public void onBecameBackground();
        public void onApplicationDestroyed();
    }

    private boolean isInBackground = true;
    private final List<Listener> listeners = new ArrayList<Listener>();
    private final Handler lifecycleDelayHandler = new Handler();
    private Runnable lifecycleTransition;

    public static ApplicationLifecycleManager get(final Application application) {
        if (instance == null) {
            instance = new ApplicationLifecycleManager(application);
        }
        return instance;
    }

    private ApplicationLifecycleManager(final Application application) {
        application.registerActivityLifecycleCallbacks(this);
    }

    public void registerListener(Listener listener) {
        listeners.add(listener);
    }

    public void unregisterListener(Listener listener) {
        listeners.remove(listener);
    }

    public boolean isInBackground() {
        return isInBackground;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (lifecycleTransition != null) {
            lifecycleDelayHandler.removeCallbacks(lifecycleTransition);
            lifecycleTransition = null;
        }

        if (isInBackground) {
            isInBackground = false;
            notifyOnBecameForeground();
            Log.i("AppLifecycleManager", "Application went to foreground");
        }
    }

    private void notifyOnBecameForeground() {
        for (Listener listener : listeners) {
            try {
                listener.onBecameForeground();
            } catch (Exception e) {
                Log.e("AppLifecycleManager", e.getMessage(), e);
            }
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if (!isInBackground && lifecycleTransition == null) {
            lifecycleTransition = new Runnable() {
                @Override
                public void run() {
                    isInBackground = true;
                    lifecycleTransition = null;
                    notifyOnBecameBackground();
                    Log.i("AppLifecycleManager", "Application went to background");
                }
            };
            lifecycleDelayHandler.postDelayed(lifecycleTransition, LIFECYCLE_DELAY);
        }
    }

    private void notifyOnApplicationDestroyed() {
        for (Listener listener : listeners) {
            try {
                listener.onApplicationDestroyed();
            } catch (Exception e) {
                Log.e("AppLifecycleManager", e.getMessage(), e);
            }
        }
    }

    private void notifyOnBecameBackground() {
        for (Listener listener : listeners) {
            try {
                listener.onBecameBackground();
            } catch (Exception e) {
                Log.e("AppLifecycleManager", e.getMessage(), e);
            }
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i("AppLifecycleManager", "Application stopped");
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.i("AppLifecycleManager", "Application created: " + savedInstanceState);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i("AppLifecycleManager", "Application started");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i("AppLifecycleManager", "Application save instance state: " + outState);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        listeners.get(0).onApplicationDestroyed();
        Log.i("AppLifecycleManager", "Application destroyed");
//        lifecycleTransition = new Runnable() {
//            @Override
//            public void run() {
//                notifyOnApplicationDestroyed();
//                Log.i("AppLifecycleManager", "Application destroyed");
//            }
//        };
//        lifecycleDelayHandler.postDelayed(lifecycleTransition, 1500);
    }
}
