package mobile.com.headsup.enums;

public enum DeviceStatus {

	ONLINE,
	OFFLINE,
	STANDBY;
	
	public static DeviceStatus fromKey(final String str) {
		try {
			return DeviceStatus.valueOf(str.toUpperCase());
		} catch (Exception e) {
			return null;
		}		
	}
	
	public String getKey() {
		return name();
	}
}
