package mobile.com.headsup.enums;

public enum FrameDelimiters {

	LF("\n"),
	NULL("\0");
	
	private final String value;
	
	private FrameDelimiters(final String value) {
		this.value = value;
	}
	
	public String getKey() {
		return name();
	}
	
	public String getValue() {
		return value;
	}
}
