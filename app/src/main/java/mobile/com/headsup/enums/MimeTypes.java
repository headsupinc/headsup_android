package mobile.com.headsup.enums;

public enum MimeTypes {

	EMPTY(""),
	TEXT_PLAIN("text/plain"),
	OCTET_STREAM("application/octet-stream");
	
	private final String value;
	
	private MimeTypes(final String value) {
		this.value = value;
	}
	
	public static MimeTypes fromKey(final String str) {
		try {
			return MimeTypes.valueOf(str.toUpperCase());
		} catch (Exception e) {
			return EMPTY;
		}		
	}
	
	public static MimeTypes fromValue(final String str) {
		try {
			MimeTypes[] types = MimeTypes.values();
			for (MimeTypes type : types) {
				if (type.getValue().equalsIgnoreCase(str)) {
					return type;
				}
			}
			return EMPTY;
		} catch (Exception e) {
			return EMPTY;
		}		
	}
	
	public String getKey() {
		return name();
	}
	
	public String getValue() {
		return value;
	}
}
