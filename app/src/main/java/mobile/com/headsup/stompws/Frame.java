package mobile.com.headsup.stompws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.com.headsup.enums.FrameDelimiters;
import mobile.com.headsup.utils.ByteUtils;
import mobile.com.headsup.utils.StringUtils;

public class Frame {

    private String command;
    private Map<String, String> headers;
    private byte[] payload;

    /**
     * Constructor of a Frame object. All parameters of a frame can be instantiate
     *
     * @param command
     * @param headers
     * @param payload
     */
    public Frame(String command, Map<String, String> headers, byte[] payload) {
        this.command = command;
        this.headers = headers != null ? headers : new HashMap<String, String>();
        this.payload = payload != null ? payload : new byte[0];
    }

    public String getCommand() {
        return command;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public byte[] getPayload() {
        return payload;
    }

    /**
     * Transform a frame object into a String. This method is copied on the objective C one, in the MMPReactiveStompClient
     * library
     * @return a frame object convert in a String
     */
    private byte[] toByteArray() {
        byte[] arr = this.command.getBytes();
        arr = ByteUtils.concat(arr, FrameDelimiters.LF.getValue().getBytes());

        for(String key : this.headers.keySet()) {
            final String header = key + ":" + this.headers.get(key) + FrameDelimiters.LF.getValue();
            arr = ByteUtils.concat(arr, header.getBytes());
        }

        arr = ByteUtils.concat(arr, FrameDelimiters.LF.getValue().getBytes());
        arr = ByteUtils.concat(arr, this.payload);
        arr = ByteUtils.concat(arr, FrameDelimiters.NULL.getValue().getBytes());

        return arr;
    }

    /**
     * Create a frame from a received message. This method is copied on the objective C one, in the MMPReactiveStompClient
     * library
     * 
     * @param payload
     *  a part of the message received from network, which represented a frame
     * @return
     *  An object frame
     */
    public static Frame fromByteArray(byte[] payload) {
        final String data = ByteUtils.convertToString(payload);

        List<String> contents = new ArrayList<String>(Arrays.asList(data.split(FrameDelimiters.LF.getValue())));

        while(contents.size() > 0 && contents.get(0).equals(StringUtils.EMPTY)){
            contents.remove(0);
        }

        String command = StringUtils.EMPTY;
        Map<String, String> headers = new HashMap<String, String>();
        String body = StringUtils.EMPTY;
        if (contents.size() > 0) {
            command = contents.get(0);

            contents.remove(0);
            boolean hasHeaders = false;
            for (String line : contents) {
                if (hasHeaders) {
                    for (int i = 0; i < line.length(); i++) {
                        Character c = line.charAt(i);
                        if (!c.equals('\0'))
                            body += c;
                    }
                } else {
                    if (line.equals("")) {
                        hasHeaders = true;
                    } else {
                        String[] header = line.split(":");
                        headers.put(header[0], header[1]);
                    }
                }
            }
        }
        return new Frame(command, headers, body.getBytes());
    }

//    No need this method, a single frame will be always be send because body of the message will never be excessive
//    /**
//     * Transform a message received from server in a Set of objects, named frame, manageable by java
//     * 
//     * @param datas
//     *        message received from network
//     * @return
//     *        a Set of Frame
//     */
//    public static Set<Frame> unmarshall(String datas){
//      String data;
//      String[] ref = datas.split(Byte.NULL + Byte.LF + "*");//NEED TO VERIFY THIS PARAMETER
//      Set<Frame> results = new HashSet<Frame>();
//      
//      for (int i = 0, len = ref.length; i < len; i++) {
//            data = ref[i];
//            
//            if ((data != null ? data.length() : 0) > 0){
//              results.add(unmarshallSingle(data));//"unmarshallSingle" is the old name method for "fromString"
//            }
//        }         
//      return results;
//    }

    /**
     * Create a frame with based fame component and convert them into a string
     * 
     * @param command
     * @param headers
     * @param payload
     * @return  a frame object convert in a String, thanks to <code>toStringg()</code> method
     */
    public static byte[] marshall(String command, Map<String, String> headers, byte[] payload){
        Frame frame = new Frame(command, headers, payload);
        return frame.toByteArray();
    }
}
