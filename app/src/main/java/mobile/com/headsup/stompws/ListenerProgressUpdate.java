package mobile.com.headsup.stompws;

import java.util.Map;

/**
 * Created by vxk on 10/16/2015.
 */
public interface ListenerProgressUpdate {
    public void onProgressUpdate(final String messageId, final float from, final float to);
}
