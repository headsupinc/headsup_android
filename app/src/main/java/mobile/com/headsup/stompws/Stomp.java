package mobile.com.headsup.stompws;

import android.util.Log;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mobile.com.headsup.utils.ByteUtils;
import mobile.com.headsup.utils.StringUtils;

//import com.neovisionaries.ws.client.WebSocket;
//import android.util.Log;
/*
import de.roderick.weberknecht.WebSocket;
import de.roderick.weberknecht.WebSocketEventHandler;
import de.roderick.weberknecht.WebSocketMessage;
*/
public class Stomp { //implements Parcelable {

    private static final String TAG = Stomp.class.getSimpleName();

    public static final int CONNECTED = 1;//Connection completely established
    public static final int NOT_AGAIN_CONNECTED = 2;//Connection process is ongoing
    public static final int DECONNECTED_FROM_OTHER = 3;//Error, no more internet connection, etc.
    public static final int DECONNECTED_FROM_APP = 4;//application explicitely ask for shut down the connection 

    private static final String PREFIX_ID_SUBSCIPTION = "sub-";
    private static final String ACCEPT_VERSION_NAME = "accept-version";
    private static final String ACCEPT_VERSION = "1.1,1.0";
    private static final String COMMAND_CONNECT = "CONNECT";
    private static final String COMMAND_CONNECTED = "CONNECTED";
    private static final String COMMAND_MESSAGE = "MESSAGE";
    private static final String COMMAND_RECEIPT = "RECEIPT";
    private static final String COMMAND_ACK = "ACK";
    private static final String COMMAND_ERROR = "ERROR";
    private static final String COMMAND_DISCONNECT = "DISCONNECT";
    private static final String COMMAND_SEND = "SEND";
    private static final String COMMAND_SUBSCRIBE = "SUBSCRIBE";
    private static final String COMMAND_UNSUBSCRIBE = "UNSUBSCRIBE";
    private static final String SUBSCRIPTION_ID = "id";
    private static final String SUBSCRIPTION_DESTINATION = "destination";
    private static final String SUBSCRIPTION_SUBSCRIPTION = "subscription";


    private static final Set<String> VERSIONS = new HashSet<String>();
    static {
        VERSIONS.add("V1.0");
        VERSIONS.add("V1.1");
        VERSIONS.add("V1.2");
    }

    private WebSocket websocket;

    private int counter;

    private int connection;

    private Map<String, String> headers;

    private int maxWebSocketFrameSize;

    private int sendBufferSize;

    private Map<String, Subscription> subscriptions;

    private ListenerWSNetwork networkListener;

    private Map<String, HashMap<String, HashMap<String, byte[]>>> messages;

    private Map<String, Thread> messageTasks;
    /**
     * Constructor of a stomp object. Only url used to set up a connection with a server can be instantiate
     * 
     * @param url
     *      the url of the server to connect with
     */
    public Stomp(String url, Map<String,String> headersSetup, ListenerWSNetwork stompStates){       
        try {
            this.websocket = new WebSocket(new URI(url), null, headersSetup);
            this.counter = 0;

            this.headers = new HashMap<String, String>();
            this.maxWebSocketFrameSize = 8 * 1024;
            this.sendBufferSize = 1024 * 1024;
            this.connection = NOT_AGAIN_CONNECTED;
            this.networkListener = stompStates;
            this.networkListener.onState(NOT_AGAIN_CONNECTED);
            this.subscriptions = new HashMap<String, Subscription>();
            this.messages = new HashMap<String, HashMap<String, HashMap<String, byte[]>>>();
            this.messageTasks = new HashMap<String, Thread>();

            this.websocket.setEventHandler(new WebSocketEventHandler() {        
                @Override
                public void onOpen(){
                    if(Stomp.this.headers != null){                                         
                        Stomp.this.headers.put(ACCEPT_VERSION_NAME, ACCEPT_VERSION);

                        transmit(COMMAND_CONNECT, Stomp.this.headers, null);

                        Log.d(TAG, "...Web Socket Openned");
                    }
                }

                @Override
                public void onMessage(WebSocketMessage message) {
                    Log.d(TAG, "<<< " + new String(message.getMessage()));
                    Frame frame = Frame.fromByteArray(message.getMessage());
                    boolean isMessageConnected = false;

                    if(frame.getCommand().equals(COMMAND_CONNECTED)){
                        Stomp.this.connection = CONNECTED;
                        Stomp.this.networkListener.onState(CONNECTED);

                        Log.d(TAG, "connected to server : " + frame.getHeaders().get("server"));
                        isMessageConnected = true;

                    } else if(frame.getCommand().equals(COMMAND_MESSAGE)){
                        String subscription = frame.getHeaders().get(SUBSCRIPTION_SUBSCRIPTION);
                        ListenerSubscription onReceive = Stomp.this.subscriptions.get(subscription).getCallback();

                        if(onReceive != null) {
                            /*
                            if (!isReceiptAcknowledged(frame.getHeaders())) {
                                final byte[] body = collateMessages(frame.getHeaders(), frame.getPayload());
                                if (body != null) {
                                    onReceive.onMessage(frame.getHeaders(), body);
                                } else {
                                    acknowledgeReceipt(frame.getHeaders());
                                }
                            }
                            */
                            final byte[] body = collateMessages(frame.getHeaders(), frame.getPayload());
                            if (body != null) {
                                onReceive.onMessage(frame.getHeaders(), body);
//                                final Map<String, String> rheaders = new HashMap<String, String>();
//                                rheaders.put("message-id", frame.getHeaders().get("message-id"));
//                                rheaders.put("subscription", frame.getHeaders().get("subscription"));
//                                transmit("ACK", rheaders, null);
                            } else {
                                acknowledgeReceipt(frame.getHeaders());
                            }
                        } else{
                            Log.e(TAG, "Error : Subscription with id = " + subscription + " had not been subscribed");
                            //ACTION TO DETERMINE TO MANAGE SUBSCRIPTION ERROR
                        }
                    } else if (frame.getCommand().equals(COMMAND_ACK)) {
                        final String messageId = headers.get("messageId");
                        if (!StringUtils.isEmpty(messageId)) {
                            final Thread thread = messageTasks.get(headers.get(messageId));
                            if (thread != null) {
                                thread.notify();
                            }
                        }
//                        Thread thread = messageTasks.get(frame.getHeaders().get("messageId"));
//                        thread.notify();
                    } else if (frame.getCommand().equals(COMMAND_RECEIPT)) {
                        final String receiptId = headers.get("receipt-id");
                        if (!StringUtils.isEmpty(receiptId)) {
                            final Thread thread = messageTasks.get(headers.get(receiptId));
                            if (thread != null) {
                                thread.notify();
                            }
                        }
//                        Thread thread = messageTasks.get(frame.getHeaders().get("messageId"));
//                        thread.notify();
                    } else if (frame.getCommand().equals(COMMAND_ERROR)) {
                        Log.e(TAG, "Error : Headers = " + frame.getHeaders() + ", Body = " + new String(frame.getPayload()));
                        //ACTION TO DETERMINE TO MANAGE ERROR MESSAGE

                    } else {

                    }

                    if(isMessageConnected)
                        Stomp.this.subscribe();
                }

                @Override
                public void onClose(){
                    if(connection == DECONNECTED_FROM_APP){
                        Log.d(TAG, "Web Socket disconnected");
                        disconnectFromApp();
                    } else{
                        Log.w(TAG, "Problem : Web Socket disconnected whereas Stomp disconnect method has never been called.");
                        disconnectFromServer();
                    }
                }

                @Override
                public void onPing() {
                	int i = 0;
                }

                @Override
                public void onPong() {
                	int i = 0;
                }

                @Override
                public void onError(IOException e) {
                	String msg = e.getMessage();
                    Log.e(TAG, "Error : " + e.getMessage());
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a message to server thanks to websocket
     * 
     * @param command
     *      one of a frame property, see {@link Frame} for more details
     * @param headers
     *      one of a frame property, see {@link Frame} for more details
     * @param payload
     *      one of a frame property, see {@link Frame} for more details
     */
    public void transmit(String command, Map<String, String> headers, byte[] payload) {
        byte[] out = Frame.marshall(command, headers, payload);

        Log.d(TAG, ">>> " + new String(out));

        int frameCount = (int) Math.ceil(Double.valueOf(out.length) / Double.valueOf(this.maxWebSocketFrameSize));
        System.out.println("frameCount: " + frameCount);
        if (out.length > this.maxWebSocketFrameSize && payload != null) {
            out = payload;
        }

        for (int i = 0; i < frameCount; i++) {
            if (payload != null) {
                if (headers != null) {
                    headers.put("frameIndex", String.valueOf(i + 1));
                    headers.put("frameCount", String.valueOf(frameCount));
                }
                this.websocket.send(Frame.marshall(command, headers, Arrays.copyOfRange(payload, i * this.maxWebSocketFrameSize, (i + 1 == frameCount) ? payload.length : (i + 1) * this.maxWebSocketFrameSize))); // partial
            } else {
                this.websocket.send(out);
            }
        }
    }

    private byte[] collateMessages(Map<String, String> headers, byte[] payload) {
        byte[] out = null;
        final String count = headers.get("messageCount");
        if (!StringUtils.isEmpty(count)) {
            final int messageCount = Integer.parseInt(headers.get("messageCount"));
            System.out.println("messageCount: " + messageCount);
            if (messageCount > 0) {
                final String messageId = headers.get("messageId");
                System.out.println("messageId: " + messageId);
                if (!StringUtils.isEmpty(messageId)) {
                    final String messageIndex = headers.get("messageIndex");
                    System.out.println("messageIndex: " + messageIndex);
                    if (!StringUtils.isEmpty(messageIndex)) {
                        HashMap<String, HashMap<String, byte[]>> partialMessages = messages.get(messageId);
                        HashMap<String, byte[]> partialFrames = new HashMap<String, byte[]>();
                        System.out.println("messages: " + messages);
                        if (partialMessages == null) {
                            partialMessages = new HashMap<String, HashMap<String, byte[]>>();
                            partialFrames = collateFrames(partialFrames, headers, payload);
                            partialMessages.put(messageIndex, partialFrames);
                            System.out.println("Added partial messages: " + partialMessages);
                            messages.put(messageId, partialMessages);
                            System.out.println("messages: " + messages);
                        } else {
                            partialFrames = partialMessages.get(messageIndex);
                            if (partialFrames == null) {
                                partialFrames = collateFrames(new HashMap<String, byte[]>(), headers, payload);
                                partialMessages.put(messageIndex, partialFrames);
                                System.out.println("Added new partial frames: " + partialMessages);
                            } else {
                                collateFrames(partialFrames, headers, payload);
                                System.out.println("Updated partial frames: " + partialMessages);
                            }
                            System.out.println("messages: " + messages);
                        }
                        final int frameCount = Integer.parseInt(headers.get("frameCount"));
                        if (messageCount == partialMessages.keySet().size() && frameCount == partialFrames.keySet().size()) {
                            System.out.println("This is the last partial frame: " + partialFrames);
                            out = ByteUtils.collateMessages(partialMessages);
                            System.out.println("out: " + out);
                        }
                    }
                }
            }
        }
        System.out.println("returning out: "+out);
        return out;
    }

    private HashMap<String, byte[]> collateFrames(HashMap<String, byte[]> partialFrames, Map<String, String> headers, byte[] payload) {
        final String frameIndex = headers.get("frameIndex");
        System.out.println("frameIndex: " + frameIndex);
        if (!StringUtils.isEmpty(frameIndex)) {
            System.out.println("partialFrames: "+partialFrames);
            if (partialFrames == null) {
                partialFrames = new HashMap<String, byte[]>();
            }
            partialFrames.put(frameIndex, payload);
            System.out.println("Added a partial frame: "+partialFrames);
        }
        System.out.println("returning partialFrames: "+partialFrames);
        return partialFrames;
    }

    private boolean isReceiptAcknowledged(final Map<String, String> headers) {
        boolean isReceiptAcknowledged = false;
        final String receiptId = headers.get("receiptId");
        if (!StringUtils.isEmpty(receiptId)) {
            final Thread thread = messageTasks.get(headers.get(receiptId));
            if (thread != null) {
                thread.notify();
                isReceiptAcknowledged = true;
            }
        }
        return isReceiptAcknowledged;
    }

    private void acknowledgeReceipt(final Map<String, String> headers) {
        final String messageId = headers.get("messageId");
        if (!StringUtils.isEmpty(messageId)) {
            final HashMap<String, HashMap<String, byte[]>> partialMessages = messages.get(messageId);
            if (partialMessages != null) {
                final String messageIndex = headers.get("messageIndex");
                if (!StringUtils.isEmpty(messageIndex)) {
                    try {
//                        final Map<String, String> rheaders = new HashMap<String, String>();
//                        rheaders.put("id", messageId);
//                        transmit("ACK", rheaders, null);
//                        final int totalSizeOfMessages = ByteUtils.computeTotalSizeOfMessages(partialMessages);
//                        if (totalSizeOfMessages == this.sendBufferSize * Integer.parseInt(messageIndex)) {
//                            String sessionId = headers.get("sessionId");
//                            String destination = headers.get(SUBSCRIPTION_DESTINATION).replaceFirst("\\d+", sessionId);
//                            //headers.put("receiptId", messageId);
//                            headers.put("receipt-id", messageId);
//                            //send(destination, COMMAND_RECEIPT, headers, null);
//                            transmit(COMMAND_RECEIPT, headers, null);
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    /*
    private byte[] collate(Map<String, String> headers, byte[] payload) {
        byte[] out = null;
        int frameCount = Integer.parseInt(headers.get("frameCount"));
        System.out.println("frameCount: "+frameCount);
        if (frameCount > 1) {
            String messageId = headers.get("messageId");
            System.out.println("messageId: "+messageId);
            if (messageId != null && messageId.length() > 0) {
                String frameIndex = headers.get("frameIndex");
                System.out.println("frameIndex: "+frameIndex);
                if (frameIndex != null && frameIndex.length() > 0) {
                    HashMap<String, byte[]> partialMessage = partialMessages.get(messageId);
                    System.out.println("partialMessages: "+partialMessages);
                    if (partialMessage == null) {
                        partialMessage = new HashMap<String, byte[]>();
                        partialMessage.put(frameIndex, payload);
                        System.out.println("Adding a partial message: "+partialMessage);
                        partialMessages.put(messageId, partialMessage);
                        System.out.println("After add partialMessages: "+partialMessages);
                    } else {
                        partialMessage.put(frameIndex, payload);
                        System.out.println("Updated a partial message: "+partialMessage);
                        if (frameCount == partialMessage.keySet().size()) {
                            System.out.println("This is the last partial message: "+partialMessage);
                            out = ByteUtils.concat(partialMessage);
                            System.out.println("out: "+out);
                        }
                    }
                }
            }
        } else {
            out = payload;
            System.out.println("out: "+out);
        }
        System.out.println("returning out: "+out);
        return out;
    }
    */
    /**
     * Set up a web socket connection with a server
     */
    public void connect(){
        if(this.connection != CONNECTED){
            Log.d(TAG, "Opening Web Socket...");
            try{
                this.websocket.connect();
            } catch (Exception e){
                Log.w(TAG, "Impossible to establish a connection : " + e.getClass() + ":" + e.getMessage());
            }
        }
    }

    /**
     * disconnection come from the server, without any intervention of client side. Operations order is very important
     */
    private void disconnectFromServer(){
        if(this.connection == CONNECTED){
            this.connection = DECONNECTED_FROM_OTHER;
            this.websocket.close();
            this.networkListener.onState(this.connection);
        }
    }

    /**
     * disconnection come from the app, because the public method disconnect was called
     */
    private void disconnectFromApp(){
        if(this.connection == DECONNECTED_FROM_APP){
            this.websocket.close();
            this.networkListener.onState(this.connection);
        }
    }

    /**
     * Close the web socket connection with the server. Operations order is very important
     */
    public void disconnect(){
        if(this.connection == CONNECTED){
            this.connection = DECONNECTED_FROM_APP;
            transmit(COMMAND_DISCONNECT, null, null);
        }
    }

    public MessageTask sendMessage(final ListenerProgressUpdate progressUpdateListener, final String destination, final Map<String,String> headers, final byte[] payload) throws Exception {
//        final int messageCount = (int) Math.ceil(Double.valueOf(payload.length) / Double.valueOf(this.sendBufferSize));
//        headers.put("messageCount", String.valueOf(messageCount));
//        System.out.println("messageCount: " + messageCount);
//        for (int i = 0; i < messageCount; i++) {
//            headers.put("messageIndex", String.valueOf(i + 1));
//            System.out.println("messageIndex: " + String.valueOf(i + 1));
//            byte[] message = Arrays.copyOfRange(payload, i * this.sendBufferSize, (i + 1 == messageCount) ? payload.length : (i + 1) * this.sendBufferSize);
//            this.send(destination, headers, message, 0);
//            TimeUnit.SECONDS.sleep(timeout);
//        }
        /*
        Runnable messageTask = new Runnable() {
            private final Object lock = new Object();
            private volatile boolean suspend = false , stopped = false;

            @Override
            public void run() {
                int messageCount = (int) Math.ceil(Double.valueOf(payload.length) / Double.valueOf(sendBufferSize));
                headers.put("messageCount", String.valueOf(messageCount));
                System.out.println("messageCount: " + messageCount);

                for (int i = 0; i < messageCount && !suspend && !stopped; i++) {
                    headers.put("messageIndex", String.valueOf(i + 1));
                    System.out.println("messageIndex: " + String.valueOf(i + 1));
                    byte[] message = Arrays.copyOfRange(payload, i * sendBufferSize, (i + 1 == messageCount) ? payload.length : (i + 1) * sendBufferSize);
                    send(destination, headers, message, 0);
                    suspend();
                }
                synchronized (lock){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                }
            }

            public void suspend(){
                suspend = true;
            }

            public void stop(){
                suspend = true;
                stopped = true;
                synchronized (lock){
                    lock.notifyAll();
                }
            }

            public void resume(){
                suspend = false;
                synchronized (lock){
                    lock.notifyAll();
                }
            }
        };
        */
        //MessageTask messageTask = new MessageTask(destination, headers, payload);
        //messageTask.run();
        final MessageTask messageTask = new MessageTask(progressUpdateListener, destination, headers, payload);
        Thread thread = new Thread(messageTask);
        thread.start();
        messageTasks.put(headers.get("messageId"), thread);
//        if (!Thread.State.TERMINATED.equals(thread.getState())) {
//
//        }

//        CompletionService<Result> ecs
//                = new ExecutorCompletionService<Result>(e);
//        ecs.submit(new TestCallable());
//        if (ecs.take().get() != null) {
//            // on finish
//        }
        return messageTask;
    }
    /**
     * Send a simple message to the server thanks to the body parameter
     * 
     * 
     * @param destination
     *      The destination through a Stomp message will be send to the server
     * @param headers
     *      headers of the message
     * @param payload
     *      body of a message
     */
    public void send(String destination, Map<String,String> headers, byte[] payload){
        if(this.connection == CONNECTED){
            if(headers == null)
                headers = new HashMap<String, String>();

            if(payload == null)
                payload = new byte[0];

            headers.put(SUBSCRIPTION_DESTINATION, destination);

            transmit(COMMAND_SEND, headers, payload);
        }
    }

    public void send(String destination, String command, Map<String,String> headers, byte[] payload) {
        if(this.connection == CONNECTED){
            if(headers == null)
                headers = new HashMap<String, String>();

            if(payload == null)
                payload = new byte[0];

            headers.put(SUBSCRIPTION_DESTINATION, destination);

            transmit(command, headers, payload);
        }
    }
    /**
     * Allow a client to send a subscription message to the server independently of the initialization of the web socket.
     * If connection have not been already done, just save the subscription
     * 
     * @param subscription
     *      a subscription object
     */
    public void subscribe(Subscription subscription){
        subscription.setId(PREFIX_ID_SUBSCIPTION + this.counter++);
        this.subscriptions.put(subscription.getId(), subscription);

        if(this.connection == CONNECTED){   
            Map<String, String> headers = new HashMap<String, String>();            
            headers.put(SUBSCRIPTION_ID, subscription.getId());
            headers.put(SUBSCRIPTION_DESTINATION, subscription.getDestination());

            subscribe(headers);
        }
    }

    /**
     * Subscribe to a Stomp channel, through messages will be send and received. A message send from a determine channel
     * can not be receive in an another.
     *
     */
    private void subscribe(){
        if(this.connection == CONNECTED){
            for(Subscription subscription : this.subscriptions.values()){
                Map<String, String> headers = new HashMap<String, String>();            
                headers.put(SUBSCRIPTION_ID, subscription.getId());
                headers.put(SUBSCRIPTION_DESTINATION, subscription.getDestination());
                subscribe(headers);
            }
        }
    }

    /**
     * Send the subscribe to the server with an header
     * @param headers
     *      header of a subscribe STOMP message
     */
    private void subscribe(Map<String, String> headers){
        //headers.put("ACK", "client");//client-individual
        transmit(COMMAND_SUBSCRIBE, headers, null);
    }

    /**
     * Destroy a subscription with its id
     * 
     * @param id
     *      the id of the subscription. This id is automatically setting up in the subscribe method
     */
    public void unsubscribe(String id){
        if(this.connection == CONNECTED){
            Map<String, String> headers = new HashMap<String, String>();
            headers.put(SUBSCRIPTION_ID, id);

            this.subscriptions.remove(id);
            this.transmit(COMMAND_UNSUBSCRIBE, headers, null);
        }
    }
/*
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //dest.writeValue(websocket);
        dest.writeParcelable(websocket, flags);
        dest.writeInt(counter);
        dest.writeInt(connection);
        dest.writeMap(headers);
        dest.writeInt(maxWebSocketFrameSize);
        dest.writeMap(subscriptions);
        //dest.writeValue(networkListener);
        //dest.writeParcelable(networkListener, flags);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Stomp> CREATOR = new Parcelable.Creator<Stomp>() {
        public Stomp createFromParcel(Parcel in) {
            return new Stomp(in);
        }

        public Stomp[] newArray(int size) {
            return new Stomp[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Stomp(Parcel in) {
        websocket = in.readParcelable(WebSocket.class.getClassLoader());
        counter = in.readInt();
        connection = in.readInt();
        headers = in.readHashMap(HashMap.class.getClassLoader());
        maxWebSocketFrameSize = in.readInt();
        subscriptions = in.readHashMap(HashMap.class.getClassLoader());
        //networkListener = (ListenerWSNetwork) in.readValue(ListenerWSNetwork.class.getClassLoader());
    }
    */

    public class MessageTask implements Runnable {

        private final Object lock = new Object();
        private volatile boolean suspend = false , stopped = false;

        private String destination;
        private Map<String,String> headers;
        private byte[] payload;
        private ListenerProgressUpdate progressUpdateListener;

        public MessageTask(final ListenerProgressUpdate progressUpdateListener, final String destination, final Map<String,String> headers, final byte[] payload) {
            this.progressUpdateListener = progressUpdateListener;
            this.destination = destination;
            this.headers = headers;
            this.payload = payload;
        }

        @Override
        public void run() {
            int messageCount = (int) Math.ceil(Double.valueOf(payload.length) / Double.valueOf(sendBufferSize));
            headers.put("messageCount", String.valueOf(messageCount));
            System.out.println("messageCount: " + messageCount);

            for (int i = 0; i < messageCount && !suspend && !stopped; i++) {
                headers.put("messageIndex", String.valueOf(i + 1));
                System.out.println("messageIndex: " + String.valueOf(i + 1));
                int start = i * sendBufferSize;
                int end = (i + 1 == messageCount) ? payload.length : (i + 1) * sendBufferSize;
                byte[] message = Arrays.copyOfRange(payload, start, end);
                send(destination, headers, message);

                if (progressUpdateListener != null) {
                    progressUpdateListener.onProgressUpdate(headers.get("messageId"), new Float((start / payload.length)) * 100f, new Float((end / payload.length)) * 100f);
                }
//                if (i + 1 < messageCount) {
//                    suspend();
//                }
                if (i + 1 < messageCount) {
                    synchronized (lock) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    }
                }
            }
        }

        public void suspend(){
            suspend = true;
        }

        public void stop(){
            suspend = true;
            stopped = true;
            synchronized (lock){
                lock.notifyAll();
            }
        }

        public void resume(){
            suspend = false;
            synchronized (lock){
                lock.notifyAll();
            }
        }

//        public void setOnProgressUpdateListener(final ListenerProgressUpdate progressUpdateListener) {
//            this.progressUpdateListener = progressUpdateListener;
//        }
    }
}
