/*
 *  Copyright (C) 2012 Roderick Baier
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License. 
 */

package mobile.com.headsup.stompws;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class WebSocketReceiver extends Thread { //implements Parcelable {
	private DataInputStream input = null;
	private WebSocket websocket = null;
	private WebSocketEventHandler eventHandler = null;

	private volatile boolean stop = false;

	
	public WebSocketReceiver(DataInputStream input, WebSocket websocket)
	{
		this.input = input;
		this.websocket = websocket;
		this.eventHandler = websocket.getEventHandler();
	}

	public void run()
	{
		List<Byte> messageBytes = new ArrayList<Byte>();

		while (!stop) {
			try {
				byte b = input.readByte();
				byte opcode = (byte) (b & 0xf);
				byte length = input.readByte();
				long payload_length = 0;
				if (length < 126) {
					payload_length = length;
				} else if (length == 126) {
					payload_length = ((0xff & input.readByte()) << 8) | (0xff & input.readByte());
				} else if (length == 127) {
					// Does work up to MAX_VALUE of long (2^63-1) after that minus values are returned.
					// However frames with such a high payload length are vastly unrealistic.
					// TODO: add Limit for WebSocket Payload Length.
					payload_length = input.readLong();
				}
				for (int i = 0; i < payload_length; i++) {
					messageBytes.add(input.readByte());
				}

				Byte[] message = messageBytes.toArray(new Byte[messageBytes.size()]);
				byte[] payload = new byte[messageBytes.size()];
				for (int i = 0; i < messageBytes.size(); i++) {
					payload[i] = message[i];
				}

				WebSocketMessage ws_message = new WebSocketMessage(payload);
				eventHandler.onMessage(ws_message);
				messageBytes.clear();
			} catch (IOException ioe) {
				handleError();
			}
		}
	}

	public void stopit()
	{
		stop = true;
	}

	public boolean isRunning()
	{
		return !stop;
	}

	private void handleError()
	{
		stopit();
		websocket.handleReceiverError();
	}
/*
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
//		dest.writeValue(input);
		dest.writeParcelable(websocket, flags);
		//dest.writeValue(eventHandler);
	}

	// this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
	public static final Parcelable.Creator<WebSocketReceiver> CREATOR = new Parcelable.Creator<WebSocketReceiver>() {
		public WebSocketReceiver createFromParcel(Parcel in) {
			return new WebSocketReceiver(in);
		}

		public WebSocketReceiver[] newArray(int size) {
			return new WebSocketReceiver[size];
		}
	};

	// example constructor that takes a Parcel and gives you an object populated with it's values
	private WebSocketReceiver(Parcel in) {
//		input = (DataInputStream) in.readValue(DataInputStream.class.getClassLoader());
		websocket = in.readParcelable(WebSocket.class.getClassLoader());
		//eventHandler = (WebSocketEventHandler) in.readValue(WebSocketEventHandler.class.getClassLoader());
	}
	*/
}
