package mobile.com.headsup.stompws;

import java.util.Map;

public interface ListenerSubscription {
	public void onMessage(Map<String, String> headers, byte[] payload);

	public void onMessageStream(Map<String, String> headers, byte[] payload);
}
