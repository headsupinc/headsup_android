package mobile.com.headsup.stompws;

import android.os.Parcelable;

public interface ListenerWSNetwork {
	public void onState(int state);
}
